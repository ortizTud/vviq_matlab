# VIVIDNESS OF VISUAL IMAGERY QUESTIONNAIRE (VVIQ)

Marks, D. F. (1973). Visual imagery differences in the recall of pictures. British journal of Psychology, 64(1), 17-24.

## Original (English)

For each item on this questionnaire, try to form a visual image, and consider your experience carefully.  For any image that you do experience, rate how vivid it is using the five-point scale described below.  If you do not have a visual image, rate vividness as ‘1’.  Only use ‘5’ for images that are truly as lively and vivid as real seeing.  Please note that there are no right or wrong answers to the questions, and that it is not necessarily desirable to experience imagery or, if you do, to have more vivid imagery.

1. Perfectly clear and vivid as real seeing.
2. Clear and reasonably vivid.
3. Moderately clear and lively.
4. Vague and dim.
5. No image at all, you only “know” that you are thinking of the object.

For items 1-4, think of some relative or friend whom you frequently see (but who is not with you at present) and consider carefully the picture that comes before your mind’s eye.

1.	The exact contour of face, head, shoulders and body.
2.	Characteristic poses of head, attitudes of body etc.
3.	The precise carriage, length of step etc., in walking.
4.	The different colours worn in some familiar clothes.

Visualise a rising sun.  Consider carefully the picture that comes before your mind’s eye.

5.	The sun rising above the horizon into a hazy sky
6.	The sky clears and surrounds the sun with blueness
7.	Clouds.  A storm blows up with flashes of lightning
8.	A rainbow appears					

Think of the front of a shop which you often go to.  Consider the picture that comes before your mind’s eye.

9.	The overall appearance of the shop from the opposite side of the road	
10.	A window display including colours, shapes and details of individual items for sale
11. You are near the entrance. The colour, shape and details of the door.	
12. You enter the shop and go to the counter. The counter assistant serves you. Money changes hands

Finally think of a country scene which involves trees, mountains and a lake.  Consider the picture that comes before your mind’s eye.

13.	The contours of the landscape
14. 	The colour and shape of the trees
15.	the colour and shape of the lake
16.	A strong wind blows on the trees and on the lake causing waves in the water.	

## Translated (German)
Fragebogen zur Lebhaftigkeit mentaler Vorstellungen Basierend auf „VVIQ – Vividness of visual imagery“

Bitte versuchen Sie, sich jede der nachfolgenden Beschreibungen dieses Fragebogens bildhaft vorzustellen, und denken Sie über ihre Vorstellung nach. Bewerten Sie jeweils wie lebhaft Ihre Vorstellung war, indem Sie die nachfolgend beschriebene 5-stufige Skala verwenden. Sollten Sie sich kein visuelles Bild vorstellen können, bewerten Sie die Lebhaftigkeit mit einer „1“. Verwenden Sie „5“ nur, wenn Ihre Vorstellung wirklich so lebhaft und klar ist, als betrachteten Sie das Objekt in der Realität. Bitte beachten Sie, dass es keine richtigen und falschen Antworten auf die Fragen gibt. Es ist ebenso nicht notwendigerweise wünschenswert, dass man visuelle Vorstellungen erlebt oder dass die Vorstellungen sehr lebhaft sind.

5-stufige Skala:

1. Absolut klar und so lebhaft wie echtes Sehen
2. Klar und ziemlich lebhaft
3. Mittelmäßig klar und lebhaft
4. Unklar und trüb
5. Gar kein Bild, Sie „wissen“ nur, dass Sie an das Objekt denken

Für die ersten vier Items denken Sie an einen Verwandten oder einen Freund, den Sie häufig sehen (der aber im Moment nicht anwesend ist). Betrachten Sie sorgfältig das Bild, das vor Ihrem inneren Auge erscheint.

1.	Die genaue Kontur von Gesicht, Kopf, Schultern und Körper.
2.	Charakteristische Haltungen des Kopfes, des Körpers etc.
3.	Der genaue Gang, Schrittlänge etc., während des Laufens.
4.	Die verschiedenen Farben vertrauter Kleidungsstücke.

Stellen Sie sich eine aufgehende Sonne vor. Betrachten Sie sorgfältig das Bild vor Ihrem inneren Auge.

5.	Die Sonne geht vor dem Horizont auf in einen dunstigen Himmel.
6.	Der Himmel klart auf und umgibt die Sonne mit Blau.
7.	Wolken. Ein Sturm zieht auf und es blitzt.
8.	Ein Regenbogen erscheint.

Denken Sie an die Vorderseite eines Ladens, in den Sie oft gehen. Betrachten Sie sorgfältig das Bild, das vor Ihrem inneren Auge entsteht.

9.	Der gesamte Anblick des Ladens von der anderen Seite der Straße aus.
10.	Ein Schaufenster mit seinen Farben, Form und den Details von bestimmten Waren.
11. Sie stehen nahe am Eingang. Farbe, Form und Details der Eingangstür.
12. Sie betreten den Laden und gehen zur Theke. Die oder der Angestellte bedient Sie. Geld wird hin und her gereicht.

Denken Sie abschließend an eine Szene auf dem Land, in der es Bäume, Berge und einen See gibt. Betrachten Sie sorgfältig das Bild, das vor Ihrem inneren Auge entsteht.

13.	Die Konturen der Landschaft.
14. Die Farbe und Form der Bäume.
15.	Die Farbe und Form des Sees.
16.	Ein starker Wind bläst durch die Bäume und über den See und verursacht Wellen im Wasser.