import time
import sys
from random import *
import random
import os
from psychopy import visual,event,core,gui
#import pyaudio
#import audioop

filepath = 'C:\\Users\\Javier\\Google Drive\\Reward-learning\\'

#---------------------------
#Setting up mic stuff
#p = pyaudio.PyAudio() #instantiate pyaudio/setting it up
#Threshold = 400

#----------------------------
#Obtain subject number/info
userVar = {'1. Subject':'Enter','4. Age':'Enter age','5. Gender':'M or F','6. Handedness':'R or L','7. Age learned English':'Age or b'}
dlg = gui.DlgFromDict(userVar)
subject = int(userVar['1. Subject'])
age = (userVar['4. Age'])
gender = userVar['5. Gender'].lower()

if gender == 'f':
    gender = '1'
elif gender == 'm':
    gender = '2'

if subject %8 ==0:
    condition = 8
else:
    condition = subject %8

if subject % 2 == 0:
    colorHigh = 'green'
    colorLow = 'red'
else:
    colorHigh = 'red'
    colorLow = 'green'
hand = userVar['6. Handedness'].upper()

english = userVar['7. Age learned English']
if english == 'b' or english == 'B':
    english = english.lower()
else:
    english = str(english)
if 12 < condition: 
    print 'Error: Condition not valid.'
    sys.exit()
#Use this to create an output file

if not os.path.isdir('Data'):
    os.makedirs('Data')  # if this fails (e.g. permissions) we will get error

study_output_name = 'Data'+os.path.sep+'reward_exp2_'+(userVar['1. Subject'])+'_phase_1.txt'
test_output_name = 'Data'+os.path.sep+'reward_exp2_'+(userVar['1. Subject'])+'_phase_2.txt'
study_output = open(study_output_name, 'a')
study_output.write('{}\t{}\n'.format('Age: ',age))
study_output.write('{}\t{}\n'.format('Gender: ',gender))
study_output.write('{}\t{}\n'.format('Hand: ',hand))
study_output.write('{}\t{}\n'.format('English: ',english))
study_output.write('Subject\tPhase\tCondition\tTrial\tItem\tItem Type\tReward\tAccuracy\tRT\tAvg_RT\tAccum_Reward\n')
test_output = open(test_output_name, 'a')
test_output.write('Subject\tPhase\tCondition\tTrial\tTargetID\tItem Type\tReward\tOvsN\tRecogACC\tRecogResp\tRecogRT\tRKResp\tRKRT\n')

#-------------------------------
#Define objects and instruction trials
win = visual.Window([1920, 1080],fullscr=True,color="black", units='pix')
instrStim = visual.TextStim(win,text='instructions', height=24, color='white',pos=[0,0],font='Lucida Console', wrapWidth=700) #make wrap width 1500 for real expt
my_clock = core.Clock() #main clock to use
second_clock = core.Clock() #used when clock needs to be reset for timing purposes (but cannot be reset for RT purposes)
win.setMouseVisible(False)

#Instructions for study phase
study_instr_green = '''Please read the following instructions carefully. 





In this experiment, you will be presented with a single word in the centre of the screen. Your task is to read the word aloud as quickly and accurately as possible. 





Each trial will start with a fixation cross in the centre of the screen. Please try to keep your eyes fixed there for the whole experiment. 





If you read the word fast enough, you will earn a monetary reward. 



For each GREEN word, you will earn 20 cents. For each RED word, you will earn 1 cent. 





At the end of the experiment, your score will be totaled and you will receive that amount in cash.





If you have any questions, please ask your experimenter for clarification.





Please press the SPACEBAR to begin.'''

#Instructions for study phase
study_instr_red = '''Please read the following instructions carefully. 





In this experiment, you will be presented with a single word in the centre of the screen. Your task is to read the word aloud as quickly and accurately as possible. 





Each trial will start with a fixation cross in the centre of the screen. Please try to keep your eyes fixed there for the whole experiment.





If you read the word fast enough, you will earn a monetary reward. 



For each RED word, you will earn 20 cents. For each GREEN word, you will earn 1 cent.





At the end of the experiment, your score will be totaled and you will receive that amount in cash.







If you have any questions, please ask your experimenter for clarification.





Please press the SPACEBAR to begin.'''

#Instructions before practice
pre_practice_instr = '''Please complete the next four practice trials following the instructions you were given.















Press the SPACEBAR to continue.'''

#Instructions after practice
post_practice_instr = '''You will now begin the experiment. Please remember that both the speed AND accuracy of your responses are important.













If you have any questions, please ask your experimenter. Press the SPACEBAR to continue.'''

#Instructions after study phase
post_study_instr = '''Please wait for your experimenter to give you your next task.







Press the SPACEBAR to continue.'''

#Instructions for math test
math_instr = '''You will be presented with a series of basic math questions. Do the best you can to complete as many questions as you can within ten minutes.





You will be provided with scrap paper on which to do your work. Calculators cannot be used.





Please type out your answer (using either the TOP ROW OF NUMBERS or NUMBER PAD on the keyboard.) As you type, you will see your answer at the bottom of the screen. Remainders can be indicated by typing R. Press ENTER to submit your answer.





You can skip questions by pressing ENTER without an answer. If you do, you will be prompted to answer the question again later on.





If you have any questions, please ask your experimenter for clarification.





Press the SPACEBAR to begin.'''

#For if test is done under 10 minutes
post_math_instr = '''Please find your experimenter before moving on to the next phase.'''

#Instructions for test phase 1
test_instr_1 = '''Please read the following instructions carefully. 





In this phase, you will be presented with a red word. For each word, please identify whether or not you had read the word in the previous phase. 







Please press the A BUTTON if you read the word in the previous phase, and you think the word is 'old'. Please press the L BUTTON if you did not read the word in the previous phase, and you think the word is 'new'.







At the bottom of the screen you will see the word OLD on the left side of the screen and the word NEW on the right side of the screen to help you remember which button to press.







Please press the SPACEBAR to continue with instructions.'''

#Instructions for test phase 2
test_instr_2 = '''If you identify a word as being 'old' or having been read in the previous phase of the experiment, you will be asked to indicate if you have a detailed memory of reading the word (Type A judgement) or if you just have a feeling that you saw it, but do not remember specifics from the experience (Type B judgement.) 







Press the A BUTTON for a Type A judgment.







Press the L BUTTON for a Type B judgment. 







Once again, there will be reminders for each response at the bottom of the screen.







Please press the SPACEBAR to continue with instructions.'''
#Instructions for test phase 3

test_instr_3 = '''Type A judgements: 





If your recognition of the word is accompanied by a conscious recollection of its prior occurrence in the study list, then indicate this by pressing the A BUTTON. 'Type A' memory is the ability to become consciously aware again of some aspect or aspects of what happened or what was experienced at the time the word was presented (e.g., aspects of the physical appearance of the word, or of something that happened in the room, or of what you were thinking and doing at the time). 







In other words, the 'Type A' word should bring back to mind a particular association, image, or something more personal from the time of study, or something about its appearance or position (i.e., what came before or after that word).







Please press the SPACEBAR to continue.'''
#Instructions for test phase 4

test_instr_4 = '''Type B judgements: 





'Type B' responses should be made when you recognize that the word was in the study list but you cannot consciously recollect anything about its actual occurrence or what happened or what was experienced at the time of its occurrence. 





In other words, press the L BUTTON (for 'Type B') when you are certain of recognizing the words but these words fail to evoke any specific conscious recollection from the study list.







Please press the SPACEBAR to continue.'''
#Instructions for test phase 5

test_instr_5 = '''To further clarify the difference between these two judgements (i.e., 'A' vs. 'B'), here are a few examples: 







If someone asks for your name, you would typically respond in the 'Type B' sense, without becoming consciously aware of anything about a particular event or experience. However, when asked about the last movie you saw, you would typically respond in the 'Type A' sense, where you become consciously aware of some aspects of the experience. 







Before you begin the memory test, please describe 'Type A' and 'Type B' judgements to the experimenter.







After you have described the judgments to the experimenter IN YOUR OWN WORDS, please press the SPACEBAR to begin.'''

#Instructions for test phase 6
test_instr_6 = '''STOP!







Have you described the difference between Type A and B judgements to the experimenter?







If not, please do so now.







If so, you may continue on to the experiment.'''

#Instructions after test phase
thank_you_instr = '''You have completed the experiment.



















Thank you for your participation.'''

#Define experimental objects
fixation = visual.TextStim(win,text='+', height=32, color='white',pos=[-200,-300],font='Lucida Console')
word1Stim = visual.TextStim(win,text='W O R D 1', height=43, color='white',pos=[-12,7],font='Lucida Console') #target
wordStim = visual.TextStim(win,text='W O R D 2', height=43, color='white',font='Lucida Console') #for math task
rewardStim = visual.TextStim(win,text='', height=43, color='white',pos=[-12,100],font='Lucida Console') #reward feedback #check if 100 is okay
old = visual.TextStim(win,text='OLD', height=32, color='white',pos=[-200,-300],font='Lucida Console') #was -400,-500 for benq
new = visual.TextStim(win,text='NEW', height=32, color='white',pos=[200,-300],font='Lucida Console') #was 400,-500 for benq
left = visual.TextStim(win,text='LEFT', height=32, color='white',pos=[-200,-300],font='Lucida Console') #was -400,-500 for benq
right = visual.TextStim(win,text='RIGHT', height=32, color='white',pos=[200,-300],font='Lucida Console') #was 400,-500 for benq
typeA = visual.TextStim(win,text='TYPE A', height=32, color='white',pos=[-200,-300],font='Lucida Console') #was -400,-500 for benq
typeB = visual.TextStim(win,text='TYPE B', height=32, color='white',pos=[200,-300],font='Lucida Console') #was 400,-500 for benq
ansStim = visual.TextStim(win,text='W O R D 1', height=44, color='white',pos=[0,-300],font='Lucida Console') #math stuff
root = 'C:\\Users\\Javier\\Google Drive\\Proyectos doctorado\\Reward-learning\\Exp3\\Stimuli\\'

#Define word lists
rHighBl_list1 = ['EMPTY', 'KNOCK', 'CLASS', 'JUICE', 'LIGHT', 'TRUCK', 'BIRTH', 'SWEET', 'FENCE', 'SPORT', 'STAMP', 'CHIEF', 'TREAT', 'SPEED', 'ROUND', 'BRIDE', 'STATE', 'SIGHT', 'CHILD', 'LEMON', 'GRANT', 'SCENE', 'JUDGE', 'PORCH', 'TRUST', 'WASTE', 'HEART', 'LUNCH', 'GUESS', 'ROUGH']
rHighBl_list2 = ['GRASS', 'KNIFE', 'BATHE', 'SUGAR', 'SLICE', 'FRUIT', 'MOTOR', 'UPPER', 'SMART', 'SHORE', 'SPOKE', 'MONEY', 'TIMER', 'BRIEF', 'FLUSH', 'TRADE', 'NERVE', 'SHELL', 'FRAME', 'PLANT', 'PRICE', 'SENSE', 'POINT', 'CABIN', 'STEEL', 'RIGHT', 'TOUCH', 'UNDER', 'SERVE', 'LOCAL']
rHighBl_list3 = ['CHEEK', 'BOARD', 'MORAL', 'MODEL', 'COURT', 'STICK', 'ISSUE', 'PLATE', 'PARTY', 'HOUSE', 'STYLE', 'SHIRT', 'PHONE', 'COUNT', 'MIGHT', 'PRIDE', 'PRESS', 'FLOUR', 'FLASH', 'SMOKE', 'SLEEP', 'YOUTH', 'PLANE', 'MOVIE', 'CLIMB', 'SKIRT', 'DRIVE', 'SHOUT', 'TOTAL', 'BREAK']
rHighBl_list4 = ['FIELD', 'STUDY', 'CRACK', 'STORE', 'WATCH', 'NURSE', 'RANGE', 'COAST', 'PAUSE', 'QUIET', 'STARE', 'IDEAL', 'SHINE', 'SHARE', 'BRAIN', 'GROUP', 'GUARD', 'LAUGH', 'SMILE', 'QUICK', 'SPOIL', 'WOUND', 'THING', 'EXTRA', 'BLIND', 'FORCE', 'EARTH', 'CLOUD', 'MONTH', 'APPLE']
rHighBl_list5 = ['TRACK', 'HORSE', 'LIMIT', 'OWNER', 'BENCH', 'PAINT', 'PAPER', 'SOUND', 'SMALL', 'FLAME', 'CROSS', 'STUFF', 'CHEST', 'NOISE', 'MATCH', 'TRAIN', 'DRESS', 'STRIP', 'SWING', 'LEAST', 'EVENT', 'HURRY', 'STAGE', 'SMELL', 'TABLE', 'WOMAN', 'STORM', 'CATCH', 'WORLD', 'OTHER']
rHighBl_list6 = ['SHEET', 'CLAIM', 'BOUND', 'ALARM', 'FAINT', 'CLOCK', 'ASIDE', 'CLOTH', 'SALAD', 'CHILL', 'START', 'MUSIC', 'BURST', 'CHAIR', 'CRASH', 'FLOOR', 'WORST', 'WAGON', 'COVER', 'DANCE', 'PLANK', 'ONION', 'FRONT', 'TRAIL', 'DAILY', 'HOTEL', 'RIVER', 'VOICE', 'SHARP', 'THROW']
rHighBl_list7 = ['OFFER', 'GUIDE', 'CHARM', 'HONEY', 'TRACE', 'WHEEL', 'STILL', 'BREAD', 'DRIFT', 'CHECK', 'TASTE', 'DREAM', 'SHORT', 'ORDER', 'PRIZE', 'SAUCE', 'RANCH', 'TODAY', 'NIGHT', 'WHILE', 'SHADE', 'VISIT', 'REACH', 'STOCK', 'MOUTH', 'GLASS', 'TEETH', 'MAJOR', 'POUND', 'TRICK']
rHighBl_list8 = ['WATER', 'SHAPE', 'CLEAN', 'CLERK', 'SPACE', 'ACTOR', 'LEAVE', 'DRINK', 'CROWD', 'BLOCK', 'BRUSH', 'LINEN', 'TWIST', 'FROWN', 'LEVEL', 'TRUTH', 'RADIO', 'VALUE', 'CRUSH', 'STORY', 'PIECE', 'UNCLE', 'GUEST', 'PRINT', 'SCORE', 'EQUAL', 'STAND', 'DOUBT', 'STONE', 'SHOCK']

rHighCl_list1 = ['WATER', 'SHAPE', 'CLEAN', 'CLERK', 'SPACE', 'ACTOR', 'LEAVE', 'DRINK', 'CROWD', 'BLOCK', 'BRUSH', 'LINEN', 'TWIST', 'FROWN', 'LEVEL', 'TRUTH', 'RADIO', 'VALUE', 'CRUSH', 'STORY', 'PIECE', 'UNCLE', 'GUEST', 'PRINT', 'SCORE', 'EQUAL', 'STAND', 'DOUBT', 'STONE', 'SHOCK']
rHighCl_list2 = ['EMPTY', 'KNOCK', 'CLASS', 'JUICE', 'LIGHT', 'TRUCK', 'BIRTH', 'SWEET', 'FENCE', 'SPORT', 'STAMP', 'CHIEF', 'TREAT', 'SPEED', 'ROUND', 'BRIDE', 'STATE', 'SIGHT', 'CHILD', 'LEMON', 'GRANT', 'SCENE', 'JUDGE', 'PORCH', 'TRUST', 'WASTE', 'HEART', 'LUNCH', 'GUESS', 'ROUGH']
rHighCl_list3 = ['GRASS', 'KNIFE', 'BATHE', 'SUGAR', 'SLICE', 'FRUIT', 'MOTOR', 'UPPER', 'SMART', 'SHORE', 'SPOKE', 'MONEY', 'TIMER', 'BRIEF', 'FLUSH', 'TRADE', 'NERVE', 'SHELL', 'FRAME', 'PLANT', 'PRICE', 'SENSE', 'POINT', 'CABIN', 'STEEL', 'RIGHT', 'TOUCH', 'UNDER', 'SERVE', 'LOCAL']
rHighCl_list4 = ['CHEEK', 'BOARD', 'MORAL', 'MODEL', 'COURT', 'STICK', 'ISSUE', 'PLATE', 'PARTY', 'HOUSE', 'STYLE', 'SHIRT', 'PHONE', 'COUNT', 'MIGHT', 'PRIDE', 'PRESS', 'FLOUR', 'FLASH', 'SMOKE', 'SLEEP', 'YOUTH', 'PLANE', 'MOVIE', 'CLIMB', 'SKIRT', 'DRIVE', 'SHOUT', 'TOTAL', 'BREAK']
rHighCl_list5 = ['FIELD', 'STUDY', 'CRACK', 'STORE', 'WATCH', 'NURSE', 'RANGE', 'COAST', 'PAUSE', 'QUIET', 'STARE', 'IDEAL', 'SHINE', 'SHARE', 'BRAIN', 'GROUP', 'GUARD', 'LAUGH', 'SMILE', 'QUICK', 'SPOIL', 'WOUND', 'THING', 'EXTRA', 'BLIND', 'FORCE', 'EARTH', 'CLOUD', 'MONTH', 'APPLE']
rHighCl_list6 = ['TRACK', 'HORSE', 'LIMIT', 'OWNER', 'BENCH', 'PAINT', 'PAPER', 'SOUND', 'SMALL', 'FLAME', 'CROSS', 'STUFF', 'CHEST', 'NOISE', 'MATCH', 'TRAIN', 'DRESS', 'STRIP', 'SWING', 'LEAST', 'EVENT', 'HURRY', 'STAGE', 'SMELL', 'TABLE', 'WOMAN', 'STORM', 'CATCH', 'WORLD', 'OTHER']
rHighCl_list7 = ['SHEET', 'CLAIM', 'BOUND', 'ALARM', 'FAINT', 'CLOCK', 'ASIDE', 'CLOTH', 'SALAD', 'CHILL', 'START', 'MUSIC', 'BURST', 'CHAIR', 'CRASH', 'FLOOR', 'WORST', 'WAGON', 'COVER', 'DANCE', 'PLANK', 'ONION', 'FRONT', 'TRAIL', 'DAILY', 'HOTEL', 'RIVER', 'VOICE', 'SHARP', 'THROW']
rHighCl_list8 = ['OFFER', 'GUIDE', 'CHARM', 'HONEY', 'TRACE', 'WHEEL', 'STILL', 'BREAD', 'DRIFT', 'CHECK', 'TASTE', 'DREAM', 'SHORT', 'ORDER', 'PRIZE', 'SAUCE', 'RANCH', 'TODAY', 'NIGHT', 'WHILE', 'SHADE', 'VISIT', 'REACH', 'STOCK', 'MOUTH', 'GLASS', 'TEETH', 'MAJOR', 'POUND', 'TRICK']

rLowBl_list1 = ['OFFER', 'GUIDE', 'CHARM', 'HONEY', 'TRACE', 'WHEEL', 'STILL', 'BREAD', 'DRIFT', 'CHECK', 'TASTE', 'DREAM', 'SHORT', 'ORDER', 'PRIZE', 'SAUCE', 'RANCH', 'TODAY', 'NIGHT', 'WHILE', 'SHADE', 'VISIT', 'REACH', 'STOCK', 'MOUTH', 'GLASS', 'TEETH', 'MAJOR', 'POUND', 'TRICK']
rLowBl_list2 = ['WATER', 'SHAPE', 'CLEAN', 'CLERK', 'SPACE', 'ACTOR', 'LEAVE', 'DRINK', 'CROWD', 'BLOCK', 'BRUSH', 'LINEN', 'TWIST', 'FROWN', 'LEVEL', 'TRUTH', 'RADIO', 'VALUE', 'CRUSH', 'STORY', 'PIECE', 'UNCLE', 'GUEST', 'PRINT', 'SCORE', 'EQUAL', 'STAND', 'DOUBT', 'STONE', 'SHOCK']
rLowBl_list3 = ['EMPTY', 'KNOCK', 'CLASS', 'JUICE', 'LIGHT', 'TRUCK', 'BIRTH', 'SWEET', 'FENCE', 'SPORT', 'STAMP', 'CHIEF', 'TREAT', 'SPEED', 'ROUND', 'BRIDE', 'STATE', 'SIGHT', 'CHILD', 'LEMON', 'GRANT', 'SCENE', 'JUDGE', 'PORCH', 'TRUST', 'WASTE', 'HEART', 'LUNCH', 'GUESS', 'ROUGH']
rLowBl_list4 = ['GRASS', 'KNIFE', 'BATHE', 'SUGAR', 'SLICE', 'FRUIT', 'MOTOR', 'UPPER', 'SMART', 'SHORE', 'SPOKE', 'MONEY', 'TIMER', 'BRIEF', 'FLUSH', 'TRADE', 'NERVE', 'SHELL', 'FRAME', 'PLANT', 'PRICE', 'SENSE', 'POINT', 'CABIN', 'STEEL', 'RIGHT', 'TOUCH', 'UNDER', 'SERVE', 'LOCAL']
rLowBl_list5 = ['CHEEK', 'BOARD', 'MORAL', 'MODEL', 'COURT', 'STICK', 'ISSUE', 'PLATE', 'PARTY', 'HOUSE', 'STYLE', 'SHIRT', 'PHONE', 'COUNT', 'MIGHT', 'PRIDE', 'PRESS', 'FLOUR', 'FLASH', 'SMOKE', 'SLEEP', 'YOUTH', 'PLANE', 'MOVIE', 'CLIMB', 'SKIRT', 'DRIVE', 'SHOUT', 'TOTAL', 'BREAK']
rLowBl_list6 = ['FIELD', 'STUDY', 'CRACK', 'STORE', 'WATCH', 'NURSE', 'RANGE', 'COAST', 'PAUSE', 'QUIET', 'STARE', 'IDEAL', 'SHINE', 'SHARE', 'BRAIN', 'GROUP', 'GUARD', 'LAUGH', 'SMILE', 'QUICK', 'SPOIL', 'WOUND', 'THING', 'EXTRA', 'BLIND', 'FORCE', 'EARTH', 'CLOUD', 'MONTH', 'APPLE']
rLowBl_list7 = ['TRACK', 'HORSE', 'LIMIT', 'OWNER', 'BENCH', 'PAINT', 'PAPER', 'SOUND', 'SMALL', 'FLAME', 'CROSS', 'STUFF', 'CHEST', 'NOISE', 'MATCH', 'TRAIN', 'DRESS', 'STRIP', 'SWING', 'LEAST', 'EVENT', 'HURRY', 'STAGE', 'SMELL', 'TABLE', 'WOMAN', 'STORM', 'CATCH', 'WORLD', 'OTHER']
rLowBl_list8 = ['SHEET', 'CLAIM', 'BOUND', 'ALARM', 'FAINT', 'CLOCK', 'ASIDE', 'CLOTH', 'SALAD', 'CHILL', 'START', 'MUSIC', 'BURST', 'CHAIR', 'CRASH', 'FLOOR', 'WORST', 'WAGON', 'COVER', 'DANCE', 'PLANK', 'ONION', 'FRONT', 'TRAIL', 'DAILY', 'HOTEL', 'RIVER', 'VOICE', 'SHARP', 'THROW']

rLowCl_list1 = ['SHEET', 'CLAIM', 'BOUND', 'ALARM', 'FAINT', 'CLOCK', 'ASIDE', 'CLOTH', 'SALAD', 'CHILL', 'START', 'MUSIC', 'BURST', 'CHAIR', 'CRASH', 'FLOOR', 'WORST', 'WAGON', 'COVER', 'DANCE', 'PLANK', 'ONION', 'FRONT', 'TRAIL', 'DAILY', 'HOTEL', 'RIVER', 'VOICE', 'SHARP', 'THROW']
rLowCl_list2 = ['OFFER', 'GUIDE', 'CHARM', 'HONEY', 'TRACE', 'WHEEL', 'STILL', 'BREAD', 'DRIFT', 'CHECK', 'TASTE', 'DREAM', 'SHORT', 'ORDER', 'PRIZE', 'SAUCE', 'RANCH', 'TODAY', 'NIGHT', 'WHILE', 'SHADE', 'VISIT', 'REACH', 'STOCK', 'MOUTH', 'GLASS', 'TEETH', 'MAJOR', 'POUND', 'TRICK']
rLowCl_list3 = ['WATER', 'SHAPE', 'CLEAN', 'CLERK', 'SPACE', 'ACTOR', 'LEAVE', 'DRINK', 'CROWD', 'BLOCK', 'BRUSH', 'LINEN', 'TWIST', 'FROWN', 'LEVEL', 'TRUTH', 'RADIO', 'VALUE', 'CRUSH', 'STORY', 'PIECE', 'UNCLE', 'GUEST', 'PRINT', 'SCORE', 'EQUAL', 'STAND', 'DOUBT', 'STONE', 'SHOCK']
rLowCl_list4 = ['EMPTY', 'KNOCK', 'CLASS', 'JUICE', 'LIGHT', 'TRUCK', 'BIRTH', 'SWEET', 'FENCE', 'SPORT', 'STAMP', 'CHIEF', 'TREAT', 'SPEED', 'ROUND', 'BRIDE', 'STATE', 'SIGHT', 'CHILD', 'LEMON', 'GRANT', 'SCENE', 'JUDGE', 'PORCH', 'TRUST', 'WASTE', 'HEART', 'LUNCH', 'GUESS', 'ROUGH']
rLowCl_list5 = ['GRASS', 'KNIFE', 'BATHE', 'SUGAR', 'SLICE', 'FRUIT', 'MOTOR', 'UPPER', 'SMART', 'SHORE', 'SPOKE', 'MONEY', 'TIMER', 'BRIEF', 'FLUSH', 'TRADE', 'NERVE', 'SHELL', 'FRAME', 'PLANT', 'PRICE', 'SENSE', 'POINT', 'CABIN', 'STEEL', 'RIGHT', 'TOUCH', 'UNDER', 'SERVE', 'LOCAL']
rLowCl_list6 = ['CHEEK', 'BOARD', 'MORAL', 'MODEL', 'COURT', 'STICK', 'ISSUE', 'PLATE', 'PARTY', 'HOUSE', 'STYLE', 'SHIRT', 'PHONE', 'COUNT', 'MIGHT', 'PRIDE', 'PRESS', 'FLOUR', 'FLASH', 'SMOKE', 'SLEEP', 'YOUTH', 'PLANE', 'MOVIE', 'CLIMB', 'SKIRT', 'DRIVE', 'SHOUT', 'TOTAL', 'BREAK']
rLowCl_list7 = ['FIELD', 'STUDY', 'CRACK', 'STORE', 'WATCH', 'NURSE', 'RANGE', 'COAST', 'PAUSE', 'QUIET', 'STARE', 'IDEAL', 'SHINE', 'SHARE', 'BRAIN', 'GROUP', 'GUARD', 'LAUGH', 'SMILE', 'QUICK', 'SPOIL', 'WOUND', 'THING', 'EXTRA', 'BLIND', 'FORCE', 'EARTH', 'CLOUD', 'MONTH', 'APPLE']
rLowCl_list8 = ['TRACK', 'HORSE', 'LIMIT', 'OWNER', 'BENCH', 'PAINT', 'PAPER', 'SOUND', 'SMALL', 'FLAME', 'CROSS', 'STUFF', 'CHEST', 'NOISE', 'MATCH', 'TRAIN', 'DRESS', 'STRIP', 'SWING', 'LEAST', 'EVENT', 'HURRY', 'STAGE', 'SMELL', 'TABLE', 'WOMAN', 'STORM', 'CATCH', 'WORLD', 'OTHER']

new_list1 = ['TRACK', 'HORSE', 'LIMIT', 'OWNER', 'BENCH', 'PAINT', 'PAPER', 'SOUND', 'SMALL', 'FLAME', 'CROSS', 'STUFF', 'CHEST', 'NOISE', 'MATCH', 'TRAIN', 'DRESS', 'STRIP', 'SWING', 'LEAST', 'EVENT', 'HURRY', 'STAGE', 'SMELL', 'TABLE', 'WOMAN', 'STORM', 'CATCH', 'WORLD', 'OTHER', 'FIELD', 'STUDY', 'CRACK', 'STORE', 'WATCH', 'NURSE', 'RANGE', 'COAST', 'PAUSE', 'QUIET', 'STARE', 'IDEAL', 'SHINE', 'SHARE', 'BRAIN', 'GROUP', 'GUARD', 'LAUGH', 'SMILE', 'QUICK', 'SPOIL', 'WOUND', 'THING', 'EXTRA', 'BLIND', 'FORCE', 'EARTH', 'CLOUD', 'MONTH', 'APPLE', 'CHEEK', 'BOARD', 'MORAL', 'MODEL', 'COURT', 'STICK', 'ISSUE', 'PLATE', 'PARTY', 'HOUSE', 'STYLE', 'SHIRT', 'PHONE', 'COUNT', 'MIGHT', 'PRIDE', 'PRESS', 'FLOUR', 'FLASH', 'SMOKE', 'SLEEP', 'YOUTH', 'PLANE', 'MOVIE', 'CLIMB', 'SKIRT', 'DRIVE', 'SHOUT', 'TOTAL', 'BREAK', 'GRASS', 'KNIFE', 'BATHE', 'SUGAR', 'SLICE', 'FRUIT', 'MOTOR', 'UPPER', 'SMART', 'SHORE', 'SPOKE', 'MONEY', 'TIMER', 'BRIEF', 'FLUSH', 'TRADE', 'NERVE', 'SHELL', 'FRAME', 'PLANT', 'PRICE', 'SENSE', 'POINT', 'CABIN', 'STEEL', 'RIGHT', 'TOUCH', 'UNDER', 'SERVE', 'LOCAL']
new_list2 = ['ASIDE', 'CLOTH', 'SALAD', 'CHILL', 'START', 'MUSIC', 'BURST', 'CHAIR', 'CRASH', 'FLOOR', 'WORST', 'WAGON', 'COVER', 'DANCE', 'PLANK', 'ONION', 'FRONT', 'TRAIL', 'DAILY', 'HOTEL', 'RIVER', 'VOICE', 'SHARP', 'THROW', 'TRACK', 'HORSE', 'LIMIT', 'OWNER', 'BENCH', 'PAINT', 'PAPER', 'SOUND', 'SMALL', 'FLAME', 'CROSS', 'STUFF', 'CHEST', 'NOISE', 'MATCH', 'TRAIN', 'DRESS', 'STRIP', 'SWING', 'LEAST', 'EVENT', 'HURRY', 'STAGE', 'SMELL', 'TABLE', 'WOMAN', 'STORM', 'CATCH', 'WORLD', 'OTHER', 'FIELD', 'STUDY', 'CRACK', 'STORE', 'WATCH', 'NURSE', 'RANGE', 'COAST', 'PAUSE', 'QUIET', 'STARE', 'IDEAL', 'SHINE', 'SHARE', 'BRAIN', 'GROUP', 'GUARD', 'LAUGH', 'SMILE', 'QUICK', 'SPOIL', 'WOUND', 'THING', 'EXTRA', 'BLIND', 'FORCE', 'EARTH', 'CLOUD', 'MONTH', 'APPLE', 'CHEEK', 'BOARD', 'MORAL', 'MODEL', 'COURT', 'STICK', 'ISSUE', 'PLATE', 'PARTY', 'HOUSE', 'STYLE', 'SHIRT', 'PHONE', 'COUNT', 'MIGHT', 'PRIDE', 'PRESS', 'FLOUR', 'FLASH', 'SMOKE', 'SLEEP', 'YOUTH', 'PLANE', 'MOVIE', 'CLIMB', 'SKIRT', 'DRIVE', 'SHOUT', 'TOTAL', 'BREAK', 'GRASS', 'KNIFE', 'BATHE', 'SUGAR', 'SLICE', 'FRUIT']
new_list3 = ['SHORT', 'ORDER', 'PRIZE', 'SAUCE', 'RANCH', 'TODAY', 'NIGHT', 'WHILE', 'SHADE', 'VISIT', 'REACH', 'STOCK', 'MOUTH', 'GLASS', 'TEETH', 'MAJOR', 'POUND', 'TRICK', 'SHEET', 'CLAIM', 'BOUND', 'ALARM', 'FAINT', 'CLOCK', 'ASIDE', 'CLOTH', 'SALAD', 'CHILL', 'START', 'MUSIC', 'BURST', 'CHAIR', 'CRASH', 'FLOOR', 'WORST', 'WAGON', 'COVER', 'DANCE', 'PLANK', 'ONION', 'FRONT', 'TRAIL', 'DAILY', 'HOTEL', 'RIVER', 'VOICE', 'SHARP', 'THROW', 'TRACK', 'HORSE', 'LIMIT', 'OWNER', 'BENCH', 'PAINT', 'PAPER', 'SOUND', 'SMALL', 'FLAME', 'CROSS', 'STUFF', 'CHEST', 'NOISE', 'MATCH', 'TRAIN', 'DRESS', 'STRIP', 'SWING', 'LEAST', 'EVENT', 'HURRY', 'STAGE', 'SMELL', 'TABLE', 'WOMAN', 'STORM', 'CATCH', 'WORLD', 'OTHER', 'FIELD', 'STUDY', 'CRACK', 'STORE', 'WATCH', 'NURSE', 'RANGE', 'COAST', 'PAUSE', 'QUIET', 'STARE', 'IDEAL', 'SHINE', 'SHARE', 'BRAIN', 'GROUP', 'GUARD', 'LAUGH', 'SMILE', 'QUICK', 'SPOIL', 'WOUND', 'THING', 'EXTRA', 'BLIND', 'FORCE', 'EARTH', 'CLOUD', 'MONTH', 'APPLE', 'CHEEK', 'BOARD', 'MORAL', 'MODEL', 'COURT', 'STICK', 'ISSUE', 'PLATE', 'PARTY', 'HOUSE', 'STYLE', 'SHIRT']
new_list4 = ['CRUSH', 'STORY', 'PIECE', 'UNCLE', 'GUEST', 'PRINT', 'SCORE', 'EQUAL', 'STAND', 'DOUBT', 'STONE', 'SHOCK', 'OFFER', 'GUIDE', 'CHARM', 'HONEY', 'TRACE', 'WHEEL', 'STILL', 'BREAD', 'DRIFT', 'CHECK', 'TASTE', 'DREAM', 'SHORT', 'ORDER', 'PRIZE', 'SAUCE', 'RANCH', 'TODAY', 'NIGHT', 'WHILE', 'SHADE', 'VISIT', 'REACH', 'STOCK', 'MOUTH', 'GLASS', 'TEETH', 'MAJOR', 'POUND', 'TRICK', 'SHEET', 'CLAIM', 'BOUND', 'ALARM', 'FAINT', 'CLOCK', 'ASIDE', 'CLOTH', 'SALAD', 'CHILL', 'START', 'MUSIC', 'BURST', 'CHAIR', 'CRASH', 'FLOOR', 'WORST', 'WAGON', 'COVER', 'DANCE', 'PLANK', 'ONION', 'FRONT', 'TRAIL', 'DAILY', 'HOTEL', 'RIVER', 'VOICE', 'SHARP', 'THROW', 'TRACK', 'HORSE', 'LIMIT', 'OWNER', 'BENCH', 'PAINT', 'PAPER', 'SOUND', 'SMALL', 'FLAME', 'CROSS', 'STUFF', 'CHEST', 'NOISE', 'MATCH', 'TRAIN', 'DRESS', 'STRIP', 'SWING', 'LEAST', 'EVENT', 'HURRY', 'STAGE', 'SMELL', 'TABLE', 'WOMAN', 'STORM', 'CATCH', 'WORLD', 'OTHER', 'FIELD', 'STUDY', 'CRACK', 'STORE', 'WATCH', 'NURSE', 'RANGE', 'COAST', 'PAUSE', 'QUIET', 'STARE', 'IDEAL', 'SHINE', 'SHARE', 'BRAIN', 'GROUP', 'GUARD', 'LAUGH']
new_list5 = ['TRUST', 'WASTE', 'HEART', 'LUNCH', 'GUESS', 'ROUGH', 'WATER', 'SHAPE', 'CLEAN', 'CLERK', 'SPACE', 'ACTOR', 'LEAVE', 'DRINK', 'CROWD', 'BLOCK', 'BRUSH', 'LINEN', 'TWIST', 'FROWN', 'LEVEL', 'TRUTH', 'RADIO', 'VALUE', 'CRUSH', 'STORY', 'PIECE', 'UNCLE', 'GUEST', 'PRINT', 'SCORE', 'EQUAL', 'STAND', 'DOUBT', 'STONE', 'SHOCK', 'OFFER', 'GUIDE', 'CHARM', 'HONEY', 'TRACE', 'WHEEL', 'STILL', 'BREAD', 'DRIFT', 'CHECK', 'TASTE', 'DREAM', 'SHORT', 'ORDER', 'PRIZE', 'SAUCE', 'RANCH', 'TODAY', 'NIGHT', 'WHILE', 'SHADE', 'VISIT', 'REACH', 'STOCK', 'MOUTH', 'GLASS', 'TEETH', 'MAJOR', 'POUND', 'TRICK', 'SHEET', 'CLAIM', 'BOUND', 'ALARM', 'FAINT', 'CLOCK', 'ASIDE', 'CLOTH', 'SALAD', 'CHILL', 'START', 'MUSIC', 'BURST', 'CHAIR', 'CRASH', 'FLOOR', 'WORST', 'WAGON', 'COVER', 'DANCE', 'PLANK', 'ONION', 'FRONT', 'TRAIL', 'DAILY', 'HOTEL', 'RIVER', 'VOICE', 'SHARP', 'THROW', 'TRACK', 'HORSE', 'LIMIT', 'OWNER', 'BENCH', 'PAINT', 'PAPER', 'SOUND', 'SMALL', 'FLAME', 'CROSS', 'STUFF', 'CHEST', 'NOISE', 'MATCH', 'TRAIN', 'DRESS', 'STRIP', 'SWING', 'LEAST', 'EVENT', 'HURRY', 'STAGE', 'SMELL']
new_list6 = ['EMPTY', 'KNOCK', 'CLASS', 'JUICE', 'LIGHT', 'TRUCK', 'BIRTH', 'SWEET', 'FENCE', 'SPORT', 'STAMP', 'CHIEF', 'TREAT', 'SPEED', 'ROUND', 'BRIDE', 'STATE', 'SIGHT', 'CHILD', 'LEMON', 'GRANT', 'SCENE', 'JUDGE', 'PORCH', 'TRUST', 'WASTE', 'HEART', 'LUNCH', 'GUESS', 'ROUGH', 'WATER', 'SHAPE', 'CLEAN', 'CLERK', 'SPACE', 'ACTOR', 'LEAVE', 'DRINK', 'CROWD', 'BLOCK', 'BRUSH', 'LINEN', 'TWIST', 'FROWN', 'LEVEL', 'TRUTH', 'RADIO', 'VALUE', 'CRUSH', 'STORY', 'PIECE', 'UNCLE', 'GUEST', 'PRINT', 'SCORE', 'EQUAL', 'STAND', 'DOUBT', 'STONE', 'SHOCK', 'OFFER', 'GUIDE', 'CHARM', 'HONEY', 'TRACE', 'WHEEL', 'STILL', 'BREAD', 'DRIFT', 'CHECK', 'TASTE', 'DREAM', 'SHORT', 'ORDER', 'PRIZE', 'SAUCE', 'RANCH', 'TODAY', 'NIGHT', 'WHILE', 'SHADE', 'VISIT', 'REACH', 'STOCK', 'MOUTH', 'GLASS', 'TEETH', 'MAJOR', 'POUND', 'TRICK', 'SHEET', 'CLAIM', 'BOUND', 'ALARM', 'FAINT', 'CLOCK', 'ASIDE', 'CLOTH', 'SALAD', 'CHILL', 'START', 'MUSIC', 'BURST', 'CHAIR', 'CRASH', 'FLOOR', 'WORST', 'WAGON', 'COVER', 'DANCE', 'PLANK', 'ONION', 'FRONT', 'TRAIL', 'DAILY', 'HOTEL', 'RIVER', 'VOICE', 'SHARP', 'THROW']
new_list7 = ['MOTOR', 'UPPER', 'SMART', 'SHORE', 'SPOKE', 'MONEY', 'TIMER', 'BRIEF', 'FLUSH', 'TRADE', 'NERVE', 'SHELL', 'FRAME', 'PLANT', 'PRICE', 'SENSE', 'POINT', 'CABIN', 'STEEL', 'RIGHT', 'TOUCH', 'UNDER', 'SERVE', 'LOCAL', 'EMPTY', 'KNOCK', 'CLASS', 'JUICE', 'LIGHT', 'TRUCK', 'BIRTH', 'SWEET', 'FENCE', 'SPORT', 'STAMP', 'CHIEF', 'TREAT', 'SPEED', 'ROUND', 'BRIDE', 'STATE', 'SIGHT', 'CHILD', 'LEMON', 'GRANT', 'SCENE', 'JUDGE', 'PORCH', 'TRUST', 'WASTE', 'HEART', 'LUNCH', 'GUESS', 'ROUGH', 'WATER', 'SHAPE', 'CLEAN', 'CLERK', 'SPACE', 'ACTOR', 'LEAVE', 'DRINK', 'CROWD', 'BLOCK', 'BRUSH', 'LINEN', 'TWIST', 'FROWN', 'LEVEL', 'TRUTH', 'RADIO', 'VALUE', 'CRUSH', 'STORY', 'PIECE', 'UNCLE', 'GUEST', 'PRINT', 'SCORE', 'EQUAL', 'STAND', 'DOUBT', 'STONE', 'SHOCK', 'OFFER', 'GUIDE', 'CHARM', 'HONEY', 'TRACE', 'WHEEL', 'STILL', 'BREAD', 'DRIFT', 'CHECK', 'TASTE', 'DREAM', 'SHORT', 'ORDER', 'PRIZE', 'SAUCE', 'RANCH', 'TODAY', 'NIGHT', 'WHILE', 'SHADE', 'VISIT', 'REACH', 'STOCK', 'MOUTH', 'GLASS', 'TEETH', 'MAJOR', 'POUND', 'TRICK', 'SHEET', 'CLAIM', 'BOUND', 'ALARM', 'FAINT', 'CLOCK']
new_list8 = ['PHONE', 'COUNT', 'MIGHT', 'PRIDE', 'PRESS', 'FLOUR', 'FLASH', 'SMOKE', 'SLEEP', 'YOUTH', 'PLANE', 'MOVIE', 'CLIMB', 'SKIRT', 'DRIVE', 'SHOUT', 'TOTAL', 'BREAK', 'GRASS', 'KNIFE', 'BATHE', 'SUGAR', 'SLICE', 'FRUIT', 'MOTOR', 'UPPER', 'SMART', 'SHORE', 'SPOKE', 'MONEY', 'TIMER', 'BRIEF', 'FLUSH', 'TRADE', 'NERVE', 'SHELL', 'FRAME', 'PLANT', 'PRICE', 'SENSE', 'POINT', 'CABIN', 'STEEL', 'RIGHT', 'TOUCH', 'UNDER', 'SERVE', 'LOCAL', 'EMPTY', 'KNOCK', 'CLASS', 'JUICE', 'LIGHT', 'TRUCK', 'BIRTH', 'SWEET', 'FENCE', 'SPORT', 'STAMP', 'CHIEF', 'TREAT', 'SPEED', 'ROUND', 'BRIDE', 'STATE', 'SIGHT', 'CHILD', 'LEMON', 'GRANT', 'SCENE', 'JUDGE', 'PORCH', 'TRUST', 'WASTE', 'HEART', 'LUNCH', 'GUESS', 'ROUGH', 'WATER', 'SHAPE', 'CLEAN', 'CLERK', 'SPACE', 'ACTOR', 'LEAVE', 'DRINK', 'CROWD', 'BLOCK', 'BRUSH', 'LINEN', 'TWIST', 'FROWN', 'LEVEL', 'TRUTH', 'RADIO', 'VALUE', 'CRUSH', 'STORY', 'PIECE', 'UNCLE', 'GUEST', 'PRINT', 'SCORE', 'EQUAL', 'STAND', 'DOUBT', 'STONE', 'SHOCK', 'OFFER', 'GUIDE', 'CHARM', 'HONEY', 'TRACE', 'WHEEL', 'STILL', 'BREAD', 'DRIFT', 'CHECK', 'TASTE', 'DREAM']

test_list1 = ['EMPTY', 'KNOCK', 'CLASS', 'JUICE', 'LIGHT', 'TRUCK', 'BIRTH', 'SWEET', 'FENCE', 'SPORT', 'STAMP', 'CHIEF', 'TREAT', 'SPEED', 'ROUND', 'BRIDE', 'STATE', 'SIGHT', 'CHILD', 'LEMON', 'GRANT', 'SCENE', 'JUDGE', 'PORCH', 'TRUST', 'WASTE', 'HEART', 'LUNCH', 'GUESS', 'ROUGH', 'WATER', 'SHAPE', 'CLEAN', 'CLERK', 'SPACE', 'ACTOR', 'LEAVE', 'DRINK', 'CROWD', 'BLOCK', 'BRUSH', 'LINEN', 'TWIST', 'FROWN', 'LEVEL', 'TRUTH', 'RADIO', 'VALUE', 'CRUSH', 'STORY', 'PIECE', 'UNCLE', 'GUEST', 'PRINT', 'SCORE', 'EQUAL', 'STAND', 'DOUBT', 'STONE', 'SHOCK', 'OFFER', 'GUIDE', 'CHARM', 'HONEY', 'TRACE', 'WHEEL', 'STILL', 'BREAD', 'DRIFT', 'CHECK', 'TASTE', 'DREAM', 'SHORT', 'ORDER', 'PRIZE', 'SAUCE', 'RANCH', 'TODAY', 'NIGHT', 'WHILE', 'SHADE', 'VISIT', 'REACH', 'STOCK', 'MOUTH', 'GLASS', 'TEETH', 'MAJOR', 'POUND', 'TRICK', 'SHEET', 'CLAIM', 'BOUND', 'ALARM', 'FAINT', 'CLOCK', 'ASIDE', 'CLOTH', 'SALAD', 'CHILL', 'START', 'MUSIC', 'BURST', 'CHAIR', 'CRASH', 'FLOOR', 'WORST', 'WAGON', 'COVER', 'DANCE', 'PLANK', 'ONION', 'FRONT', 'TRAIL', 'DAILY', 'HOTEL', 'RIVER', 'VOICE', 'SHARP', 'THROW', 'TRACK', 'HORSE', 'LIMIT', 'OWNER', 'BENCH', 'PAINT', 'PAPER', 'SOUND', 'SMALL', 'FLAME', 'CROSS', 'STUFF', 'CHEST', 'NOISE', 'MATCH', 'TRAIN', 'DRESS', 'STRIP', 'SWING', 'LEAST', 'EVENT', 'HURRY', 'STAGE', 'SMELL', 'TABLE', 'WOMAN', 'STORM', 'CATCH', 'WORLD', 'OTHER', 'FIELD', 'STUDY', 'CRACK', 'STORE', 'WATCH', 'NURSE', 'RANGE', 'COAST', 'PAUSE', 'QUIET', 'STARE', 'IDEAL', 'SHINE', 'SHARE', 'BRAIN', 'GROUP', 'GUARD', 'LAUGH', 'SMILE', 'QUICK', 'SPOIL', 'WOUND', 'THING', 'EXTRA', 'BLIND', 'FORCE', 'EARTH', 'CLOUD', 'MONTH', 'APPLE', 'CHEEK', 'BOARD', 'MORAL', 'MODEL', 'COURT', 'STICK', 'ISSUE', 'PLATE', 'PARTY', 'HOUSE', 'STYLE', 'SHIRT', 'PHONE', 'COUNT', 'MIGHT', 'PRIDE', 'PRESS', 'FLOUR', 'FLASH', 'SMOKE', 'SLEEP', 'YOUTH', 'PLANE', 'MOVIE', 'CLIMB', 'SKIRT', 'DRIVE', 'SHOUT', 'TOTAL', 'BREAK', 'GRASS', 'KNIFE', 'BATHE', 'SUGAR', 'SLICE', 'FRUIT', 'MOTOR', 'UPPER', 'SMART', 'SHORE', 'SPOKE', 'MONEY', 'TIMER', 'BRIEF', 'FLUSH', 'TRADE', 'NERVE', 'SHELL', 'FRAME', 'PLANT', 'PRICE', 'SENSE', 'POINT', 'CABIN', 'STEEL', 'RIGHT', 'TOUCH', 'UNDER', 'SERVE', 'LOCAL']
test_list2 = ['GRASS', 'KNIFE', 'BATHE', 'SUGAR', 'SLICE', 'FRUIT', 'MOTOR', 'UPPER', 'SMART', 'SHORE', 'SPOKE', 'MONEY', 'TIMER', 'BRIEF', 'FLUSH', 'TRADE', 'NERVE', 'SHELL', 'FRAME', 'PLANT', 'PRICE', 'SENSE', 'POINT', 'CABIN', 'STEEL', 'RIGHT', 'TOUCH', 'UNDER', 'SERVE', 'LOCAL', 'EMPTY', 'KNOCK', 'CLASS', 'JUICE', 'LIGHT', 'TRUCK', 'BIRTH', 'SWEET', 'FENCE', 'SPORT', 'STAMP', 'CHIEF', 'TREAT', 'SPEED', 'ROUND', 'BRIDE', 'STATE', 'SIGHT', 'CHILD', 'LEMON', 'GRANT', 'SCENE', 'JUDGE', 'PORCH', 'TRUST', 'WASTE', 'HEART', 'LUNCH', 'GUESS', 'ROUGH', 'WATER', 'SHAPE', 'CLEAN', 'CLERK', 'SPACE', 'ACTOR', 'LEAVE', 'DRINK', 'CROWD', 'BLOCK', 'BRUSH', 'LINEN', 'TWIST', 'FROWN', 'LEVEL', 'TRUTH', 'RADIO', 'VALUE', 'CRUSH', 'STORY', 'PIECE', 'UNCLE', 'GUEST', 'PRINT', 'SCORE', 'EQUAL', 'STAND', 'DOUBT', 'STONE', 'SHOCK', 'OFFER', 'GUIDE', 'CHARM', 'HONEY', 'TRACE', 'WHEEL', 'STILL', 'BREAD', 'DRIFT', 'CHECK', 'TASTE', 'DREAM', 'SHORT', 'ORDER', 'PRIZE', 'SAUCE', 'RANCH', 'TODAY', 'NIGHT', 'WHILE', 'SHADE', 'VISIT', 'REACH', 'STOCK', 'MOUTH', 'GLASS', 'TEETH', 'MAJOR', 'POUND', 'TRICK', 'SHEET', 'CLAIM', 'BOUND', 'ALARM', 'FAINT', 'CLOCK', 'ASIDE', 'CLOTH', 'SALAD', 'CHILL', 'START', 'MUSIC', 'BURST', 'CHAIR', 'CRASH', 'FLOOR', 'WORST', 'WAGON', 'COVER', 'DANCE', 'PLANK', 'ONION', 'FRONT', 'TRAIL', 'DAILY', 'HOTEL', 'RIVER', 'VOICE', 'SHARP', 'THROW', 'TRACK', 'HORSE', 'LIMIT', 'OWNER', 'BENCH', 'PAINT', 'PAPER', 'SOUND', 'SMALL', 'FLAME', 'CROSS', 'STUFF', 'CHEST', 'NOISE', 'MATCH', 'TRAIN', 'DRESS', 'STRIP', 'SWING', 'LEAST', 'EVENT', 'HURRY', 'STAGE', 'SMELL', 'TABLE', 'WOMAN', 'STORM', 'CATCH', 'WORLD', 'OTHER', 'FIELD', 'STUDY', 'CRACK', 'STORE', 'WATCH', 'NURSE', 'RANGE', 'COAST', 'PAUSE', 'QUIET', 'STARE', 'IDEAL', 'SHINE', 'SHARE', 'BRAIN', 'GROUP', 'GUARD', 'LAUGH', 'SMILE', 'QUICK', 'SPOIL', 'WOUND', 'THING', 'EXTRA', 'BLIND', 'FORCE', 'EARTH', 'CLOUD', 'MONTH', 'APPLE', 'CHEEK', 'BOARD', 'MORAL', 'MODEL', 'COURT', 'STICK', 'ISSUE', 'PLATE', 'PARTY', 'HOUSE', 'STYLE', 'SHIRT', 'PHONE', 'COUNT', 'MIGHT', 'PRIDE', 'PRESS', 'FLOUR', 'FLASH', 'SMOKE', 'SLEEP', 'YOUTH', 'PLANE', 'MOVIE', 'CLIMB', 'SKIRT', 'DRIVE', 'SHOUT', 'TOTAL', 'BREAK']
test_list3 = ['CHEEK', 'BOARD', 'MORAL', 'MODEL', 'COURT', 'STICK', 'ISSUE', 'PLATE', 'PARTY', 'HOUSE', 'STYLE', 'SHIRT', 'PHONE', 'COUNT', 'MIGHT', 'PRIDE', 'PRESS', 'FLOUR', 'FLASH', 'SMOKE', 'SLEEP', 'YOUTH', 'PLANE', 'MOVIE', 'CLIMB', 'SKIRT', 'DRIVE', 'SHOUT', 'TOTAL', 'BREAK', 'GRASS', 'KNIFE', 'BATHE', 'SUGAR', 'SLICE', 'FRUIT', 'MOTOR', 'UPPER', 'SMART', 'SHORE', 'SPOKE', 'MONEY', 'TIMER', 'BRIEF', 'FLUSH', 'TRADE', 'NERVE', 'SHELL', 'FRAME', 'PLANT', 'PRICE', 'SENSE', 'POINT', 'CABIN', 'STEEL', 'RIGHT', 'TOUCH', 'UNDER', 'SERVE', 'LOCAL', 'EMPTY', 'KNOCK', 'CLASS', 'JUICE', 'LIGHT', 'TRUCK', 'BIRTH', 'SWEET', 'FENCE', 'SPORT', 'STAMP', 'CHIEF', 'TREAT', 'SPEED', 'ROUND', 'BRIDE', 'STATE', 'SIGHT', 'CHILD', 'LEMON', 'GRANT', 'SCENE', 'JUDGE', 'PORCH', 'TRUST', 'WASTE', 'HEART', 'LUNCH', 'GUESS', 'ROUGH', 'WATER', 'SHAPE', 'CLEAN', 'CLERK', 'SPACE', 'ACTOR', 'LEAVE', 'DRINK', 'CROWD', 'BLOCK', 'BRUSH', 'LINEN', 'TWIST', 'FROWN', 'LEVEL', 'TRUTH', 'RADIO', 'VALUE', 'CRUSH', 'STORY', 'PIECE', 'UNCLE', 'GUEST', 'PRINT', 'SCORE', 'EQUAL', 'STAND', 'DOUBT', 'STONE', 'SHOCK', 'OFFER', 'GUIDE', 'CHARM', 'HONEY', 'TRACE', 'WHEEL', 'STILL', 'BREAD', 'DRIFT', 'CHECK', 'TASTE', 'DREAM', 'SHORT', 'ORDER', 'PRIZE', 'SAUCE', 'RANCH', 'TODAY', 'NIGHT', 'WHILE', 'SHADE', 'VISIT', 'REACH', 'STOCK', 'MOUTH', 'GLASS', 'TEETH', 'MAJOR', 'POUND', 'TRICK', 'SHEET', 'CLAIM', 'BOUND', 'ALARM', 'FAINT', 'CLOCK', 'ASIDE', 'CLOTH', 'SALAD', 'CHILL', 'START', 'MUSIC', 'BURST', 'CHAIR', 'CRASH', 'FLOOR', 'WORST', 'WAGON', 'COVER', 'DANCE', 'PLANK', 'ONION', 'FRONT', 'TRAIL', 'DAILY', 'HOTEL', 'RIVER', 'VOICE', 'SHARP', 'THROW', 'TRACK', 'HORSE', 'LIMIT', 'OWNER', 'BENCH', 'PAINT', 'PAPER', 'SOUND', 'SMALL', 'FLAME', 'CROSS', 'STUFF', 'CHEST', 'NOISE', 'MATCH', 'TRAIN', 'DRESS', 'STRIP', 'SWING', 'LEAST', 'EVENT', 'HURRY', 'STAGE', 'SMELL', 'TABLE', 'WOMAN', 'STORM', 'CATCH', 'WORLD', 'OTHER', 'FIELD', 'STUDY', 'CRACK', 'STORE', 'WATCH', 'NURSE', 'RANGE', 'COAST', 'PAUSE', 'QUIET', 'STARE', 'IDEAL', 'SHINE', 'SHARE', 'BRAIN', 'GROUP', 'GUARD', 'LAUGH', 'SMILE', 'QUICK', 'SPOIL', 'WOUND', 'THING', 'EXTRA', 'BLIND', 'FORCE', 'EARTH', 'CLOUD', 'MONTH', 'APPLE']
test_list4 = ['FIELD', 'STUDY', 'CRACK', 'STORE', 'WATCH', 'NURSE', 'RANGE', 'COAST', 'PAUSE', 'QUIET', 'STARE', 'IDEAL', 'SHINE', 'SHARE', 'BRAIN', 'GROUP', 'GUARD', 'LAUGH', 'SMILE', 'QUICK', 'SPOIL', 'WOUND', 'THING', 'EXTRA', 'BLIND', 'FORCE', 'EARTH', 'CLOUD', 'MONTH', 'APPLE', 'CHEEK', 'BOARD', 'MORAL', 'MODEL', 'COURT', 'STICK', 'ISSUE', 'PLATE', 'PARTY', 'HOUSE', 'STYLE', 'SHIRT', 'PHONE', 'COUNT', 'MIGHT', 'PRIDE', 'PRESS', 'FLOUR', 'FLASH', 'SMOKE', 'SLEEP', 'YOUTH', 'PLANE', 'MOVIE', 'CLIMB', 'SKIRT', 'DRIVE', 'SHOUT', 'TOTAL', 'BREAK', 'GRASS', 'KNIFE', 'BATHE', 'SUGAR', 'SLICE', 'FRUIT', 'MOTOR', 'UPPER', 'SMART', 'SHORE', 'SPOKE', 'MONEY', 'TIMER', 'BRIEF', 'FLUSH', 'TRADE', 'NERVE', 'SHELL', 'FRAME', 'PLANT', 'PRICE', 'SENSE', 'POINT', 'CABIN', 'STEEL', 'RIGHT', 'TOUCH', 'UNDER', 'SERVE', 'LOCAL', 'EMPTY', 'KNOCK', 'CLASS', 'JUICE', 'LIGHT', 'TRUCK', 'BIRTH', 'SWEET', 'FENCE', 'SPORT', 'STAMP', 'CHIEF', 'TREAT', 'SPEED', 'ROUND', 'BRIDE', 'STATE', 'SIGHT', 'CHILD', 'LEMON', 'GRANT', 'SCENE', 'JUDGE', 'PORCH', 'TRUST', 'WASTE', 'HEART', 'LUNCH', 'GUESS', 'ROUGH', 'WATER', 'SHAPE', 'CLEAN', 'CLERK', 'SPACE', 'ACTOR', 'LEAVE', 'DRINK', 'CROWD', 'BLOCK', 'BRUSH', 'LINEN', 'TWIST', 'FROWN', 'LEVEL', 'TRUTH', 'RADIO', 'VALUE', 'CRUSH', 'STORY', 'PIECE', 'UNCLE', 'GUEST', 'PRINT', 'SCORE', 'EQUAL', 'STAND', 'DOUBT', 'STONE', 'SHOCK', 'OFFER', 'GUIDE', 'CHARM', 'HONEY', 'TRACE', 'WHEEL', 'STILL', 'BREAD', 'DRIFT', 'CHECK', 'TASTE', 'DREAM', 'SHORT', 'ORDER', 'PRIZE', 'SAUCE', 'RANCH', 'TODAY', 'NIGHT', 'WHILE', 'SHADE', 'VISIT', 'REACH', 'STOCK', 'MOUTH', 'GLASS', 'TEETH', 'MAJOR', 'POUND', 'TRICK', 'SHEET', 'CLAIM', 'BOUND', 'ALARM', 'FAINT', 'CLOCK', 'ASIDE', 'CLOTH', 'SALAD', 'CHILL', 'START', 'MUSIC', 'BURST', 'CHAIR', 'CRASH', 'FLOOR', 'WORST', 'WAGON', 'COVER', 'DANCE', 'PLANK', 'ONION', 'FRONT', 'TRAIL', 'DAILY', 'HOTEL', 'RIVER', 'VOICE', 'SHARP', 'THROW', 'TRACK', 'HORSE', 'LIMIT', 'OWNER', 'BENCH', 'PAINT', 'PAPER', 'SOUND', 'SMALL', 'FLAME', 'CROSS', 'STUFF', 'CHEST', 'NOISE', 'MATCH', 'TRAIN', 'DRESS', 'STRIP', 'SWING', 'LEAST', 'EVENT', 'HURRY', 'STAGE', 'SMELL', 'TABLE', 'WOMAN', 'STORM', 'CATCH', 'WORLD', 'OTHER']
test_list5 = ['TRACK', 'HORSE', 'LIMIT', 'OWNER', 'BENCH', 'PAINT', 'PAPER', 'SOUND', 'SMALL', 'FLAME', 'CROSS', 'STUFF', 'CHEST', 'NOISE', 'MATCH', 'TRAIN', 'DRESS', 'STRIP', 'SWING', 'LEAST', 'EVENT', 'HURRY', 'STAGE', 'SMELL', 'TABLE', 'WOMAN', 'STORM', 'CATCH', 'WORLD', 'OTHER', 'FIELD', 'STUDY', 'CRACK', 'STORE', 'WATCH', 'NURSE', 'RANGE', 'COAST', 'PAUSE', 'QUIET', 'STARE', 'IDEAL', 'SHINE', 'SHARE', 'BRAIN', 'GROUP', 'GUARD', 'LAUGH', 'SMILE', 'QUICK', 'SPOIL', 'WOUND', 'THING', 'EXTRA', 'BLIND', 'FORCE', 'EARTH', 'CLOUD', 'MONTH', 'APPLE', 'CHEEK', 'BOARD', 'MORAL', 'MODEL', 'COURT', 'STICK', 'ISSUE', 'PLATE', 'PARTY', 'HOUSE', 'STYLE', 'SHIRT', 'PHONE', 'COUNT', 'MIGHT', 'PRIDE', 'PRESS', 'FLOUR', 'FLASH', 'SMOKE', 'SLEEP', 'YOUTH', 'PLANE', 'MOVIE', 'CLIMB', 'SKIRT', 'DRIVE', 'SHOUT', 'TOTAL', 'BREAK', 'GRASS', 'KNIFE', 'BATHE', 'SUGAR', 'SLICE', 'FRUIT', 'MOTOR', 'UPPER', 'SMART', 'SHORE', 'SPOKE', 'MONEY', 'TIMER', 'BRIEF', 'FLUSH', 'TRADE', 'NERVE', 'SHELL', 'FRAME', 'PLANT', 'PRICE', 'SENSE', 'POINT', 'CABIN', 'STEEL', 'RIGHT', 'TOUCH', 'UNDER', 'SERVE', 'LOCAL', 'EMPTY', 'KNOCK', 'CLASS', 'JUICE', 'LIGHT', 'TRUCK', 'BIRTH', 'SWEET', 'FENCE', 'SPORT', 'STAMP', 'CHIEF', 'TREAT', 'SPEED', 'ROUND', 'BRIDE', 'STATE', 'SIGHT', 'CHILD', 'LEMON', 'GRANT', 'SCENE', 'JUDGE', 'PORCH', 'TRUST', 'WASTE', 'HEART', 'LUNCH', 'GUESS', 'ROUGH', 'WATER', 'SHAPE', 'CLEAN', 'CLERK', 'SPACE', 'ACTOR', 'LEAVE', 'DRINK', 'CROWD', 'BLOCK', 'BRUSH', 'LINEN', 'TWIST', 'FROWN', 'LEVEL', 'TRUTH', 'RADIO', 'VALUE', 'CRUSH', 'STORY', 'PIECE', 'UNCLE', 'GUEST', 'PRINT', 'SCORE', 'EQUAL', 'STAND', 'DOUBT', 'STONE', 'SHOCK', 'OFFER', 'GUIDE', 'CHARM', 'HONEY', 'TRACE', 'WHEEL', 'STILL', 'BREAD', 'DRIFT', 'CHECK', 'TASTE', 'DREAM', 'SHORT', 'ORDER', 'PRIZE', 'SAUCE', 'RANCH', 'TODAY', 'NIGHT', 'WHILE', 'SHADE', 'VISIT', 'REACH', 'STOCK', 'MOUTH', 'GLASS', 'TEETH', 'MAJOR', 'POUND', 'TRICK', 'SHEET', 'CLAIM', 'BOUND', 'ALARM', 'FAINT', 'CLOCK', 'ASIDE', 'CLOTH', 'SALAD', 'CHILL', 'START', 'MUSIC', 'BURST', 'CHAIR', 'CRASH', 'FLOOR', 'WORST', 'WAGON', 'COVER', 'DANCE', 'PLANK', 'ONION', 'FRONT', 'TRAIL', 'DAILY', 'HOTEL', 'RIVER', 'VOICE', 'SHARP', 'THROW']
test_list6 = ['SHEET', 'CLAIM', 'BOUND', 'ALARM', 'FAINT', 'CLOCK', 'ASIDE', 'CLOTH', 'SALAD', 'CHILL', 'START', 'MUSIC', 'BURST', 'CHAIR', 'CRASH', 'FLOOR', 'WORST', 'WAGON', 'COVER', 'DANCE', 'PLANK', 'ONION', 'FRONT', 'TRAIL', 'DAILY', 'HOTEL', 'RIVER', 'VOICE', 'SHARP', 'THROW', 'TRACK', 'HORSE', 'LIMIT', 'OWNER', 'BENCH', 'PAINT', 'PAPER', 'SOUND', 'SMALL', 'FLAME', 'CROSS', 'STUFF', 'CHEST', 'NOISE', 'MATCH', 'TRAIN', 'DRESS', 'STRIP', 'SWING', 'LEAST', 'EVENT', 'HURRY', 'STAGE', 'SMELL', 'TABLE', 'WOMAN', 'STORM', 'CATCH', 'WORLD', 'OTHER', 'FIELD', 'STUDY', 'CRACK', 'STORE', 'WATCH', 'NURSE', 'RANGE', 'COAST', 'PAUSE', 'QUIET', 'STARE', 'IDEAL', 'SHINE', 'SHARE', 'BRAIN', 'GROUP', 'GUARD', 'LAUGH', 'SMILE', 'QUICK', 'SPOIL', 'WOUND', 'THING', 'EXTRA', 'BLIND', 'FORCE', 'EARTH', 'CLOUD', 'MONTH', 'APPLE', 'CHEEK', 'BOARD', 'MORAL', 'MODEL', 'COURT', 'STICK', 'ISSUE', 'PLATE', 'PARTY', 'HOUSE', 'STYLE', 'SHIRT', 'PHONE', 'COUNT', 'MIGHT', 'PRIDE', 'PRESS', 'FLOUR', 'FLASH', 'SMOKE', 'SLEEP', 'YOUTH', 'PLANE', 'MOVIE', 'CLIMB', 'SKIRT', 'DRIVE', 'SHOUT', 'TOTAL', 'BREAK', 'GRASS', 'KNIFE', 'BATHE', 'SUGAR', 'SLICE', 'FRUIT', 'MOTOR', 'UPPER', 'SMART', 'SHORE', 'SPOKE', 'MONEY', 'TIMER', 'BRIEF', 'FLUSH', 'TRADE', 'NERVE', 'SHELL', 'FRAME', 'PLANT', 'PRICE', 'SENSE', 'POINT', 'CABIN', 'STEEL', 'RIGHT', 'TOUCH', 'UNDER', 'SERVE', 'LOCAL', 'EMPTY', 'KNOCK', 'CLASS', 'JUICE', 'LIGHT', 'TRUCK', 'BIRTH', 'SWEET', 'FENCE', 'SPORT', 'STAMP', 'CHIEF', 'TREAT', 'SPEED', 'ROUND', 'BRIDE', 'STATE', 'SIGHT', 'CHILD', 'LEMON', 'GRANT', 'SCENE', 'JUDGE', 'PORCH', 'TRUST', 'WASTE', 'HEART', 'LUNCH', 'GUESS', 'ROUGH', 'WATER', 'SHAPE', 'CLEAN', 'CLERK', 'SPACE', 'ACTOR', 'LEAVE', 'DRINK', 'CROWD', 'BLOCK', 'BRUSH', 'LINEN', 'TWIST', 'FROWN', 'LEVEL', 'TRUTH', 'RADIO', 'VALUE', 'CRUSH', 'STORY', 'PIECE', 'UNCLE', 'GUEST', 'PRINT', 'SCORE', 'EQUAL', 'STAND', 'DOUBT', 'STONE', 'SHOCK', 'OFFER', 'GUIDE', 'CHARM', 'HONEY', 'TRACE', 'WHEEL', 'STILL', 'BREAD', 'DRIFT', 'CHECK', 'TASTE', 'DREAM', 'SHORT', 'ORDER', 'PRIZE', 'SAUCE', 'RANCH', 'TODAY', 'NIGHT', 'WHILE', 'SHADE', 'VISIT', 'REACH', 'STOCK', 'MOUTH', 'GLASS', 'TEETH', 'MAJOR', 'POUND', 'TRICK']
test_list7 = ['OFFER', 'GUIDE', 'CHARM', 'HONEY', 'TRACE', 'WHEEL', 'STILL', 'BREAD', 'DRIFT', 'CHECK', 'TASTE', 'DREAM', 'SHORT', 'ORDER', 'PRIZE', 'SAUCE', 'RANCH', 'TODAY', 'NIGHT', 'WHILE', 'SHADE', 'VISIT', 'REACH', 'STOCK', 'MOUTH', 'GLASS', 'TEETH', 'MAJOR', 'POUND', 'TRICK', 'SHEET', 'CLAIM', 'BOUND', 'ALARM', 'FAINT', 'CLOCK', 'ASIDE', 'CLOTH', 'SALAD', 'CHILL', 'START', 'MUSIC', 'BURST', 'CHAIR', 'CRASH', 'FLOOR', 'WORST', 'WAGON', 'COVER', 'DANCE', 'PLANK', 'ONION', 'FRONT', 'TRAIL', 'DAILY', 'HOTEL', 'RIVER', 'VOICE', 'SHARP', 'THROW', 'TRACK', 'HORSE', 'LIMIT', 'OWNER', 'BENCH', 'PAINT', 'PAPER', 'SOUND', 'SMALL', 'FLAME', 'CROSS', 'STUFF', 'CHEST', 'NOISE', 'MATCH', 'TRAIN', 'DRESS', 'STRIP', 'SWING', 'LEAST', 'EVENT', 'HURRY', 'STAGE', 'SMELL', 'TABLE', 'WOMAN', 'STORM', 'CATCH', 'WORLD', 'OTHER', 'FIELD', 'STUDY', 'CRACK', 'STORE', 'WATCH', 'NURSE', 'RANGE', 'COAST', 'PAUSE', 'QUIET', 'STARE', 'IDEAL', 'SHINE', 'SHARE', 'BRAIN', 'GROUP', 'GUARD', 'LAUGH', 'SMILE', 'QUICK', 'SPOIL', 'WOUND', 'THING', 'EXTRA', 'BLIND', 'FORCE', 'EARTH', 'CLOUD', 'MONTH', 'APPLE', 'CHEEK', 'BOARD', 'MORAL', 'MODEL', 'COURT', 'STICK', 'ISSUE', 'PLATE', 'PARTY', 'HOUSE', 'STYLE', 'SHIRT', 'PHONE', 'COUNT', 'MIGHT', 'PRIDE', 'PRESS', 'FLOUR', 'FLASH', 'SMOKE', 'SLEEP', 'YOUTH', 'PLANE', 'MOVIE', 'CLIMB', 'SKIRT', 'DRIVE', 'SHOUT', 'TOTAL', 'BREAK', 'GRASS', 'KNIFE', 'BATHE', 'SUGAR', 'SLICE', 'FRUIT', 'MOTOR', 'UPPER', 'SMART', 'SHORE', 'SPOKE', 'MONEY', 'TIMER', 'BRIEF', 'FLUSH', 'TRADE', 'NERVE', 'SHELL', 'FRAME', 'PLANT', 'PRICE', 'SENSE', 'POINT', 'CABIN', 'STEEL', 'RIGHT', 'TOUCH', 'UNDER', 'SERVE', 'LOCAL', 'EMPTY', 'KNOCK', 'CLASS', 'JUICE', 'LIGHT', 'TRUCK', 'BIRTH', 'SWEET', 'FENCE', 'SPORT', 'STAMP', 'CHIEF', 'TREAT', 'SPEED', 'ROUND', 'BRIDE', 'STATE', 'SIGHT', 'CHILD', 'LEMON', 'GRANT', 'SCENE', 'JUDGE', 'PORCH', 'TRUST', 'WASTE', 'HEART', 'LUNCH', 'GUESS', 'ROUGH', 'WATER', 'SHAPE', 'CLEAN', 'CLERK', 'SPACE', 'ACTOR', 'LEAVE', 'DRINK', 'CROWD', 'BLOCK', 'BRUSH', 'LINEN', 'TWIST', 'FROWN', 'LEVEL', 'TRUTH', 'RADIO', 'VALUE', 'CRUSH', 'STORY', 'PIECE', 'UNCLE', 'GUEST', 'PRINT', 'SCORE', 'EQUAL', 'STAND', 'DOUBT', 'STONE', 'SHOCK']
test_list8 = ['WATER', 'SHAPE', 'CLEAN', 'CLERK', 'SPACE', 'ACTOR', 'LEAVE', 'DRINK', 'CROWD', 'BLOCK', 'BRUSH', 'LINEN', 'TWIST', 'FROWN', 'LEVEL', 'TRUTH', 'RADIO', 'VALUE', 'CRUSH', 'STORY', 'PIECE', 'UNCLE', 'GUEST', 'PRINT', 'SCORE', 'EQUAL', 'STAND', 'DOUBT', 'STONE', 'SHOCK', 'OFFER', 'GUIDE', 'CHARM', 'HONEY', 'TRACE', 'WHEEL', 'STILL', 'BREAD', 'DRIFT', 'CHECK', 'TASTE', 'DREAM', 'SHORT', 'ORDER', 'PRIZE', 'SAUCE', 'RANCH', 'TODAY', 'NIGHT', 'WHILE', 'SHADE', 'VISIT', 'REACH', 'STOCK', 'MOUTH', 'GLASS', 'TEETH', 'MAJOR', 'POUND', 'TRICK', 'SHEET', 'CLAIM', 'BOUND', 'ALARM', 'FAINT', 'CLOCK', 'ASIDE', 'CLOTH', 'SALAD', 'CHILL', 'START', 'MUSIC', 'BURST', 'CHAIR', 'CRASH', 'FLOOR', 'WORST', 'WAGON', 'COVER', 'DANCE', 'PLANK', 'ONION', 'FRONT', 'TRAIL', 'DAILY', 'HOTEL', 'RIVER', 'VOICE', 'SHARP', 'THROW', 'TRACK', 'HORSE', 'LIMIT', 'OWNER', 'BENCH', 'PAINT', 'PAPER', 'SOUND', 'SMALL', 'FLAME', 'CROSS', 'STUFF', 'CHEST', 'NOISE', 'MATCH', 'TRAIN', 'DRESS', 'STRIP', 'SWING', 'LEAST', 'EVENT', 'HURRY', 'STAGE', 'SMELL', 'TABLE', 'WOMAN', 'STORM', 'CATCH', 'WORLD', 'OTHER', 'FIELD', 'STUDY', 'CRACK', 'STORE', 'WATCH', 'NURSE', 'RANGE', 'COAST', 'PAUSE', 'QUIET', 'STARE', 'IDEAL', 'SHINE', 'SHARE', 'BRAIN', 'GROUP', 'GUARD', 'LAUGH', 'SMILE', 'QUICK', 'SPOIL', 'WOUND', 'THING', 'EXTRA', 'BLIND', 'FORCE', 'EARTH', 'CLOUD', 'MONTH', 'APPLE', 'CHEEK', 'BOARD', 'MORAL', 'MODEL', 'COURT', 'STICK', 'ISSUE', 'PLATE', 'PARTY', 'HOUSE', 'STYLE', 'SHIRT', 'PHONE', 'COUNT', 'MIGHT', 'PRIDE', 'PRESS', 'FLOUR', 'FLASH', 'SMOKE', 'SLEEP', 'YOUTH', 'PLANE', 'MOVIE', 'CLIMB', 'SKIRT', 'DRIVE', 'SHOUT', 'TOTAL', 'BREAK', 'GRASS', 'KNIFE', 'BATHE', 'SUGAR', 'SLICE', 'FRUIT', 'MOTOR', 'UPPER', 'SMART', 'SHORE', 'SPOKE', 'MONEY', 'TIMER', 'BRIEF', 'FLUSH', 'TRADE', 'NERVE', 'SHELL', 'FRAME', 'PLANT', 'PRICE', 'SENSE', 'POINT', 'CABIN', 'STEEL', 'RIGHT', 'TOUCH', 'UNDER', 'SERVE', 'LOCAL', 'EMPTY', 'KNOCK', 'CLASS', 'JUICE', 'LIGHT', 'TRUCK', 'BIRTH', 'SWEET', 'FENCE', 'SPORT', 'STAMP', 'CHIEF', 'TREAT', 'SPEED', 'ROUND', 'BRIDE', 'STATE', 'SIGHT', 'CHILD', 'LEMON', 'GRANT', 'SCENE', 'JUDGE', 'PORCH', 'TRUST', 'WASTE', 'HEART', 'LUNCH', 'GUESS', 'ROUGH']

#Short word lists
#EHAH_word_list1 = ['EMPTY', 'KNOCK', 'CLASS', 'JUICE']
#EHAH_word_list2 = ['CHEEK', 'BOARD', 'MORAL', 'MODEL']
#EHAH_word_list3 = ['TRACK', 'HORSE', 'LIMIT', 'OWNER']
#EHAH_word_list4 = ['OFFER', 'GUIDE', 'CHARM', 'HONEY']
#ELAL_word_list1 = ['OFFER', 'GUIDE', 'CHARM', 'HONEY']
#ELAL_word_list2 = ['EMPTY', 'KNOCK', 'CLASS', 'JUICE']
#ELAL_word_list3 = ['CHEEK', 'BOARD', 'MORAL', 'MODEL']
#ELAL_word_list4 = ['TRACK', 'HORSE', 'LIMIT', 'OWNER']
#new_word_list1 = ['TRACK', 'HORSE', 'LIMIT', 'OWNER']
#new_word_list2 = ['OFFER', 'GUIDE', 'CHARM', 'HONEY']
#new_word_list3 = ['EMPTY', 'KNOCK', 'CLASS', 'JUICE']
#new_word_list4 = ['CHEEK', 'BOARD', 'MORAL', 'MODEL']

#Counterbalancing lists used based on condition
if condition == 1:
    old_rHighBl_list = rHighBl_list1[:]
    old_rHighCl_list = rHighCl_list1[:]
    old_rLowBl_list = rLowBl_list1[:]
    old_rLowCl_list = rLowCl_list1[:]
    new_list = new_list1[:]
elif condition == 2:
    old_rHighBl_list = rHighBl_list2[:]
    old_rHighCl_list = rHighCl_list2[:]
    old_rLowBl_list = rLowBl_list2[:]
    old_rLowCl_list = rLowCl_list2[:]
    new_list = new_list2[:]
elif condition == 3:
    old_rHighBl_list = rHighBl_list3[:]
    old_rHighCl_list = rHighCl_list3[:]
    old_rLowBl_list = rLowBl_list3[:]
    old_rLowCl_list = rLowCl_list3[:]
    new_list = new_list3[:]
elif condition == 4:
    old_rHighBl_list = rHighBl_list4[:]
    old_rHighCl_list = rHighCl_list4[:]
    old_rLowBl_list = rLowBl_list4[:]
    old_rLowCl_list = rLowCl_list4[:]
    new_list = new_list4[:]
elif condition == 5:
    old_rHighBl_list = rHighBl_list5[:]
    old_rHighCl_list = rHighCl_list5[:]
    old_rLowBl_list = rLowBl_list5[:]
    old_rLowCl_list = rLowCl_list5[:]
    new_list = new_list5[:]
elif condition == 6:
    old_rHighBl_list = rHighBl_list6[:]
    old_rHighCl_list = rHighCl_list6[:]
    old_rLowBl_list = rLowBl_list6[:]
    old_rLowCl_list = rLowCl_list6[:]
    new_list = new_list6[:]
elif condition == 7:
    old_rHighBl_list = rHighBl_list7[:]
    old_rHighCl_list = rHighCl_list7[:]
    old_rLowBl_list = rLowBl_list7[:]
    old_rLowCl_list = rLowCl_list7[:]
    new_list = new_list7[:]
elif condition == 8:
    old_rHighBl_list = rHighBl_list8[:]
    old_rHighCl_list = rHighCl_list8[:]
    old_rLowBl_list = rLowBl_list8[:]
    old_rLowCl_list = rLowCl_list8[:]
    new_list = new_list8[:]

shuffle(old_rHighBl_list)
shuffle(old_rHighCl_list)
shuffle(old_rLowBl_list)
shuffle(old_rLowCl_list)
shuffle(new_list)

#Determining order: old rHigh, rLow, blurry, clear and new lists based off of condition number
#old_rHighBl_order = []
#for i in range(len(old_rHighBl_list)): 
#    old_rHighBl_order.append(i)
#
#old_rHighCl_order = []
#for i in range(len(old_rHighCl_list)): 
#    old_rHighCl_order.append(i)
#
#old_rLowBl_order = []
#for i in range(len(old_rLowBl_list)): 
#    old_rLowBl_order.append(i)
#
#old_rLowCl_order = []
#for i in range(len(old_rLowCl_list)): 
#    old_rLowCl_order.append(i)
#
#new_list_order = []
#for i in range(len(new_list)): 
#    new_list_order.append(i)
#
#shuffle(old_rHighBl_order)
#shuffle(old_rHighCl_order)
#shuffle(old_rLowBl_order)
#shuffle(old_rLowCl_order)
#shuffle(new_list_order)

#Determining the sequence of trial types (valid vs invalid) for the study phase

studyTrials = [[0 for x in range(2)] for x in range(120)]
for i in range(120):
    if i<60:
        studyTrials[i][0]='rHigh'
    else:
        studyTrials[i][0]='rLow'

for i in range(120):
    if i<30:
        studyTrials[i][1]='blurry'
    elif i<60:
        studyTrials[i][1]='clear'
    elif i<90:
        studyTrials[i][1]='blurry'
    else:
        studyTrials[i][1]='clear'

shuffle(studyTrials)

#Determining the sequence of trial types (old vs new) for the test phase
testTrials = [[0 for x in range(3)] for x in range(240)]
for i in range(240):
    if i<120:
        testTrials[i][0]='old'
    else:
        testTrials[i][0]='new'

for i in range(240):
    if i<60:
        testTrials[i][1]='rHigh'
    elif i<120:
        testTrials[i][1]='rLow'
    elif i<180:
        testTrials[i][1]='rHigh'
    else:
        testTrials[i][1]='rLow'

for i in range(240):
    if i<30:
        testTrials[i][2]='blurry'
    elif i<60:
        testTrials[i][2]='clear'
    elif i<90:
        testTrials[i][2]='blurry'
    elif i<120:
        testTrials[i][2]='clear'
    elif i<150:
        testTrials[i][2]='blurry'
    elif i<180:
        testTrials[i][2]='clear'
    elif i<210:
        testTrials[i][2]='blurry'
    else:
        testTrials[i][2]='clear'

shuffle(testTrials)

#Practice Trials
rHighBl_practice = ['pEIGHT', 'pGLORY', 'pMIDST']
rHighCl_practice = ['pEIGHT', 'pGLORY', 'pMIDST']
rLowBl_practice = ['pSOLID', 'pQUEEN', 'pLOBBY']
rLowCl_practice = ['pSOLID', 'pQUEEN', 'pLOBBY']
practice_trials = [[0 for x in range(2)] for x in range(12)]
for i in range(12):
    if i<len(practice_trials)/2:
        practice_trials[i][0]='rHigh'
    else:
        practice_trials[i][0]='rLow'

for i in range(12):
    if i<len(practice_trials)/4:
        practice_trials[i][1]='blurry'
    elif i<(len(practice_trials)/4)*2:
        practice_trials[i][1]='clear'
    elif i<(len(practice_trials)/4)*3:
        practice_trials[i][1]='blurry'
    else:
        practice_trials[i][1]='clear'
shuffle(practice_trials)
shuffle(rHighBl_practice)
shuffle(rHighCl_practice)
shuffle(rLowBl_practice)
shuffle(rLowCl_practice)

##################################################################################################################################################################################
#BEGIN EXPERIMENT
#Present study instructions
#
#if subject % 2 == 0:
#    instrStim.setText(study_instr_green)
#else:
#    instrStim.setText(study_instr_red)
#instrStim.draw()
#
#win.flip()
#core.wait(3)
#act = event.waitKeys(10000,'space,delete')
#
#if act[0] == 'delete':
#        sys.exit()
#
#Practice instructions
#instrStim.setText(pre_practice_instr)
#instrStim.draw()
#
#win.flip()
#core.wait(2)
#act = event.waitKeys(10000,'space,delete')
#
#if act[0] == 'delete':
#        sys.exit()

#Present blank screen
win.flip()
core.wait(.15)

#creating counters 
practice_rHighBl_num = 0
practice_rLowBl_num = 0
practice_rHighCl_num = 0
practice_rLowCl_num = 0
avg_rt = 1
stimType = 0

#Practice phase loop
i=0

for trial_num in range(len(practice_trials)):

#present fixation cross
    fixation.setPos([0,0])
    fixation.draw()
    win.flip()
    core.wait(.5)

#generate trials
    if practice_trials[trial_num][0] == 'rHigh':
        if practice_trials[trial_num][1] == 'blurry':
            word=rHighBl_practice[practice_rHighBl_num]
            filepath='Blurry\\'
            picture = visual.SimpleImageStim(win, image=root+filepath+colorHigh+'\\'+word+'.jpg')
            picture.draw
            stimType='blurry'
            reward='high'
            colorTarget=colorHigh
            practice_rHighBl_num=practice_rHighBl_num+1
        else:
            word=rHighBl_practice[practice_rHighCl_num]
            filepath='Clear\\'
            picture = visual.SimpleImageStim(win, image=root+filepath+colorHigh+'\\'+word+'.jpg')
            picture.draw
            stimType='clear'
            reward='high'
            colorTarget=colorHigh
            practice_rHighCl_num=practice_rHighCl_num+1
        reward= '20'
    else:
        if practice_trials[trial_num][1] == 'blurry':
            word=rLowBl_practice[practice_rLowBl_num]
            filepath='\\Blurry\\'
            picture = visual.SimpleImageStim(win, image=root+filepath+colorLow+'\\'+word+'.jpg')
            picture.draw
            stimType='blurry'
            reward='low'
            colorTarget=colorLow
            practice_rLowBl_num=practice_rLowBl_num+1
        else:
            word=rLowBl_practice[practice_rLowCl_num]
            filepath='\\Clear\\'
            picture = visual.SimpleImageStim(win, image=root+filepath+colorLow+'\\'+word+'.jpg')
            picture.draw
            stimType='clear'
            reward='low'
            colorTarget=colorLow
            practice_rLowCl_num=practice_rLowCl_num+1
        reward= '1'

#present word
    picture.draw()
    win.flip()
    rt = 0
    my_clock.reset()
    second_clock.reset()  

#Keyboard responses -Javi's laptop
    acc_resp_raw = event.waitKeys(2,'1,delete')
    if  acc_resp_raw== None:
        rt=0
    elif acc_resp_raw[0]== '1':
         acc_resp = '1'
         rt = my_clock.getTime()
         remaining_time = 1-rt #calculating time left for stim to be presented. Fix this when mic finally works.
    elif acc_resp_raw[0] == 'delete':
         sys.exit()

#Recording vocal responses 
#    stream = p.open(rate = 44100, channels = 1, format = pyaudio.paInt16, input = True)
#    for y in range(500):
#        sound_data = stream.read(882)
#        if my_clock.getTime() > 2:
#            stream.close()
#            break
#        elif audioop.rms(sound_data, 2)>Threshold*2:
#            rt = my_clock.getTime()
#            stream.close()
#            remaining_time = 1-rt #calculating time left for stim to be presented. Fix this when mic finally works.
#            break
#    if rt != 0: #if rt is 0, then already displayed for full second
#        word1Stim.draw()
#        win.flip()
#        core.wait(remaining_time)

#displaying feedback and word
    if rt == 0:
        word1Stim.setText('No response recorded')
        word1Stim.draw()
        win.flip()
        core.wait(1)
    elif rt <= (avg_rt + avg_rt/2):
        rewardStim.setText(reward)
        rewardStim.draw()
        word1Stim.draw()
        win.flip()
        core.wait(1)
    elif rt > (avg_rt + avg_rt/2):
        word1Stim.setText('0 cents. Not fast enough!')
        word1Stim.draw()
        win.flip()
        core.wait(1)
    elif rt > 1: #no reward if response slower than 1000 ms
        word1Stim.setText('0 cents. Not fast enough!')
        word1Stim.draw()
        win.flip()
        core.wait(1)
    win.flip()
    acc_resp_raw = event.waitKeys(10000,'1,2,3,delete')
    if acc_resp_raw[0] == '1':
        acc_resp = '1'
    elif acc_resp_raw[0] == '2':
        acc_resp = '2'
    elif acc_resp_raw[0] == '3':
        acc_resp = '3'
    elif acc_resp_raw[0] == 'delete':
        sys.exit()

#Present blank screen

win.flip()
core.wait(.15)

#Post practice instructions
instrStim.setText(post_practice_instr)
instrStim.draw()

win.flip()
core.wait(2)
act = event.waitKeys(10000,'space,delete')

if act[0] == 'delete':
        sys.exit()

#Present blank screen
win.flip()
core.wait(.15)

#creating counters 
rHighBl_num = 0
rLowBl_num = 0
rHighCl_num = 0
rLowCl_num = 0
avg_rt = 1
stimType = 0
avg_rt = 1

i=0
context = [[0 for x in range(3)] for x in range(120)]
accum_reward = 0  #counter of reward amount in output 

g=0
HB=0
HC=0
LC=0
LB=0

#Study phase loop
for trial_num in range(len(studyTrials)): #for real experiment: len(studyTrials)
    location = random.randint(1,2)

#present fixation cross
    fixation.setPos([0,0])
    fixation.draw()
    win.flip()
    core.wait(.5)

#generate trials
    if studyTrials[trial_num][0] == 'rHigh':
        if studyTrials[trial_num][1] == 'blurry':
            word=old_rHighBl_list[rHighBl_num]
            contType='blurry'
            contReward='high'
            colorTarget=colorHigh
            picture = visual.SimpleImageStim(win, image=root+'\\'+contType+'\\'+colorTarget+'\\'+word+'.jpg')
            rHighBl_num=rHighBl_num+1
            HB=HB+1
        else:
            word=old_rHighCl_list[rHighCl_num]
            contType='clear'
            contReward='high'
            colorTarget=colorHigh
            picture = visual.SimpleImageStim(win, image=root+'\\'+contType+'\\'+colorTarget+'\\'+word+'.jpg')
            rHighCl_num=rHighCl_num+1
            HC=HC+1
        reward= '20'
    else:
        if studyTrials[trial_num][1] == 'blurry':
            word=old_rLowBl_list[rLowBl_num]
            contType='blurry'
            contReward='low'
            colorTarget=colorLow
            picture = visual.SimpleImageStim(win, image=root+'\\'+contType+'\\'+colorTarget+'\\'+word+'.jpg')
            rLowBl_num=rLowBl_num+1
            LB=LB+1
        else:
            word=old_rLowCl_list[rLowCl_num]
            contType='clear'
            contReward='low'
            colorTarget=colorLow
            picture = visual.SimpleImageStim(win, image=root+'\\'+contType+'\\'+colorTarget+'\\'+word+'.jpg')
            rLowCl_num=rLowCl_num+1
            LC=LC+1
        reward= '1'
        
#present word
    picture.draw()
    win.flip()
    rt = 0
    my_clock.reset()
    second_clock.reset()
    g=g+1
    print HB
    print HC
    print LB
    print LC
    print '----'

    acc_resp_raw=0
#Keyboard responses -Javi's laptop
    acc_resp_raw = event.waitKeys(2,'1,delete')
    if  acc_resp_raw== None:
        rt=0
    elif acc_resp_raw[0]== '1':
         acc_resp = '1'
         rt = my_clock.getTime()
         remaining_time = 1-rt #calculating time left for stim to be presented. Fix this when mic finally works.
    elif acc_resp_raw[0] == 'delete':
         sys.exit()

#Recording vocal responses 
#    stream = p.open(rate = 44100, channels = 1, format = pyaudio.paInt16, input = True)
#    for y in range(500):
#        sound_data = stream.read(882)
#        if my_clock.getTime() > 2:
#            stream.close()
#            break
#        elif audioop.rms(sound_data, 2)>Threshold*2:
#            rt = my_clock.getTime()
#            stream.close()
#            remaining_time = 1-rt #calculating time left for stim to be presented. Fix this when mic finally works.
#            break
#    if rt != 0: #if rt is 0, then already displayed for full second
#        word1Stim.draw()
#        win.flip()
#        core.wait(remaining_time)

    #displaying feedback and word
    if rt == 0:
        word1Stim.setText('No response recorded')
        word1Stim.draw()
        win.flip()
        core.wait(1)
        reward_given = 0
        accum_reward = accum_reward+0
    elif rt <= (avg_rt + avg_rt/2):
        rewardStim.setText(reward)
        rewardStim.draw()
        picture.draw()
        win.flip()
        core.wait(1)
        if studyTrials[trial_num][0] == 'rHigh':
            accum_reward = accum_reward+20
            reward_given = 20
        elif studyTrials[trial_num][0] == 'rLow':
            accum_reward = accum_reward+1
            reward_given = 1
    elif rt > (avg_rt + avg_rt/2):
        word1Stim.setText('0 cents. Not fast enough!')
        word1Stim.draw()
        win.flip()
        core.wait(1)
        accum_reward = accum_reward+0
    elif rt > 1: #no reward if response slower than 1000 ms
        word1Stim.setText('0 cents. Not fast enough!')
        word1Stim.draw()
        win.flip()
        core.wait(1)
        accum_reward = accum_reward+0
    win.flip()

#Coding
    acc_resp_raw = event.waitKeys(10000,'1,2,3,delete')
    if acc_resp_raw[0] == '1':
        acc_resp = '1'
    elif acc_resp_raw[0] == '2':
        acc_resp = '2'
    elif acc_resp_raw[0] == '3':
        acc_resp = '3'
    elif acc_resp_raw[0] == 'delete':
        sys.exit()

#Calculate rt criterion
    if trial_num==0:
        avg_rt = (avg_rt+rt)/2
    else:
        avg_rt = (avg_rt*(trial_num-1) + rt)/ trial_num
    trial_num+=1
    rt = rt*1000

    context[i][0]=word 
    context[i][1]=contReward
    context[i][2]=contType

    i=i+1
    study_output.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(subject, '1', str(condition), str(trial_num), word, contType, contReward, acc_resp, str(rt), avg_rt, accum_reward))

study_output.close()
print context
shuffle(context)

#Present post study instructions
instrStim.setText(post_study_instr)
instrStim.draw()
win.flip()
core.wait(2) #for actual expt, change to 550
act = event.waitKeys(10000,'space,delete')

if act[0] == 'delete':
    sys.exit()

#################################################################################################################################################################MATH TEST
#
#Present math instructions
#instrStim.setText(math_instr)
#instrStim.draw()
#win.flip()
#core.wait(2)
#act = event.waitKeys(10000,'space,delete')
#if act[0] == 'delete':
#    sys.exit()
#
#Math test 
#math_questions = ['376x43','800+5-12/4','4353/7','(1238-71)/7','842+904+719','108x77/9','(21+43-8)x32','625482/6','27+6x7','7623x67',
#'(87+1035x11)/4','53628x34','(87+90+143-56+32)/5','275+836+145-13','759x12','42+564/3-12']
#question_num = 0
#len_math_questions = 16
#my_clock.reset()
#
#while my_clock.getTime() < 600: #10 minutes = 600
#    wordStim.setText(math_questions[question_num])
#    wordStim.draw()
#    win.flip()
#    typed_word = ''
#    while event.getKeys(keyList=['return'])==[]: #until subject hits return
#            key=event.getKeys(keyList=['r','1','2','3','4','5','6','7','8','9','0','num_1','num_2','num_3','num_4','num_5','num_6','num_7','num_8','num_9','num_0','backspace'])
#            for number in key: #recording response into string
#                if number == 'backspace':
#                    typed_word = typed_word[:-1]
#                else:
#                    if number == 'num_1':
#                        number = '1'
#                    elif number == 'num_2':
#                        number = '2'
#                    elif number == 'num_3':
#                        number = '3'
#                    elif number == 'num_4':
#                        number = '4'
#                    elif number == 'num_5':
#                        number = '5'
#                    elif number == 'num_6':
#                        number = '6'
#                    elif number == 'num_7':
#                        number = '7'
#                    elif number == 'num_8':
#                        number = '8'
#                    elif number == 'num_9':
#                        number = '9'
#                    elif number == 'num_0':
#                        number = '0'
#                    typed_word+=number
#                ansStim.setText(typed_word)
#                wordStim.draw()
#                ansStim.draw()
#                win.flip()
#
#    if len(typed_word) == 0:
#        math_questions.append(math_questions[question_num])
#        question_num+=1
#        len_math_questions+=1
#    else:
#        question_num+=1
#    if question_num == len_math_questions: #need this to be a variable; may increase if questions are skipped
#        break
#instrStim.setText(post_math_instr)
#instrStim.draw()
#win.flip()
#core.wait(2)
#
#act = event.waitKeys(10000,'space,delete')
#if act[0] == 'delete':
#    sys.exit()
#
#####################################################################################################################################################TEST
#
#Present test instructions
#instrStim.setText(test_instr_1)
#instrStim.draw()
#win.flip()
#core.wait(2)
#act = event.waitKeys(10000,'space,delete')
#
#if act[0] == 'delete':
#    sys.exit()
#instrStim.setText(test_instr_2)
#instrStim.draw()
#win.flip()
#core.wait(2)
#act = event.waitKeys(10000,'space,delete')
#
#if act[0] == 'delete':
#    sys.exit()
#instrStim.setText(test_instr_3)
#instrStim.draw()
#win.flip()
#core.wait(2)
#act = event.waitKeys(10000,'space,delete')
#
#if act[0] == 'delete':
#    sys.exit()
#instrStim.setText(test_instr_4)
#instrStim.draw()
#win.flip()
#core.wait(2)
#act = event.waitKeys(10000,'space,delete')
#
#if act[0] == 'delete':
#    sys.exit()
#instrStim.setText(test_instr_5)
#instrStim.draw()
#win.flip()
#core.wait(2)
#act = event.waitKeys(10000,'space,delete')
#
#if act[0] == 'delete':
#    sys.exit()
#instrStim.setText(test_instr_6)
#instrStim.setHeight(50)
#instrStim.draw()
#win.flip()
#core.wait(2)
#act = event.waitKeys(10000,'space,delete')
#
#if act[0] == 'delete':
#    sys.exit()

#Present blank screen
win.flip()
core.wait(.15)

#Present test trials
test_num = 0
new_num = 0 
trial_num = 0  #basic variable for writing into files

for i in range(len(testTrials)):
    fixation.draw()
    win.flip()
    core.wait(2)

#generate test trials
    if testTrials[i][0] == 'old':
        word=context[test_num][0]
        if context[test_num][1]=='high':
            colorTarget=colorHigh
        else:
            colorTarget=colorLow
        reward=context[test_num][1]
        contType=context[test_num][2]
        OvsN=testTrials[i][0]
        picture = visual.SimpleImageStim(win, image=root+'\\'+contType+'\\'+colorTarget+'\\'+word+'.jpg')
        test_num=test_num+1
    else:
        word=new_list[new_num]
        if testTrials[test_num][1]=='rhigh':
            colorTarget=colorHigh
        else:
            colorTarget=colorLow
        reward=testTrials[test_num][1]
        contType=testTrials[test_num][2]
        OvsN=testTrials[i][0]
        picture = visual.SimpleImageStim(win, image=root+'\\'+contType+'\\'+colorTarget+'\\'+word+'.jpg')
        new_num=new_num+1
    
#present test words
    picture.draw()
    old.draw()
    new.draw()
    win.flip()
    
    recog_key = event.waitKeys(100000,'a,l,delete') #using l and a keys since rshift is being detected as lshift for some reason...
    recog_rt = my_clock.getTime()*1000

    if recog_key[0] == 'l':
        recog_resp = 'new' #for writing into files
        if testTrials[i][0] == 'old':
            recog_acc = '0' #variable to write into files
        else:
            recog_acc = '1'
        RK_resp = '' #setting empty variables as these are irrelvant for new responses
        RK_rt = ''
    elif recog_key[0] == 'a':
        recog_resp = 'old' #for writing into files
        if testTrials[i][0]=='old':
            recog_acc = '1'
        else:
            recog_acc = '0'
            
        picture.draw() #RK trial; old resp given
        typeA.draw()
        typeB.draw()
        win.flip()

        my_clock.reset()
        RK_key = event.waitKeys(100000,'a,l,delete') 
        RK_rt = my_clock.getTime()*1000

        if RK_key[0] == 'a':
            RK_resp = 'remember'
        elif RK_key[0] == 'l':
            RK_resp = 'know'
        elif RK_key[0] == 'delete':
            sys.exit()
    
    trial_num+=1

    test_output.write('{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n'.format(subject,'2',str(condition),str(trial_num),word,contType,reward,testTrials[i][0],recog_acc,recog_resp,str(recog_rt),RK_resp,str(RK_rt)))

test_output.close()
instrStim.setText(thank_you_instr)
instrStim.setHeight(24) 
instrStim.draw()
win.flip()
event.waitKeys(10000,'space')

sys.exit()