import time
import sys
from random import *
import random
import os
from psychopy import visual,event,core,gui

#---------------------------
# Language?
userVar = {'1. Language? : 1-German; 2-English'}
dlg = gui.DlgFromDict(userVar)
lang = int(userVar['1. Language'])
print(lang)

#---------------------------
# Load texts from file
if lang = 1:
	wordFile = "instruct_de.txt"
elif lang = 2:
	wordFile = "instruct_en.txt"
instruct = []
with open(wordFile, "r") as f:
    instruct = f.read().splitlines()

# Load texts from file
if lang = 1:
	wordFile = "block1_de.txt"
elif lang = 2:
	wordFile = "block1_en.txt"
block1 = []
with open(wordFile, "r") as f:
    block1 = f.read().splitlines()

# Load texts from file
if lang = 1:
	wordFile = "block2_de.txt"
elif lang = 2:
	wordFile = "block2_en.txt"
block2 = []
with open(wordFile, "r") as f:
    block2 = f.read().splitlines()

# Load texts from file
if lang = 1:
	wordFile = "block3_de.txt"
elif lang = 2:
	wordFile = "block3_en.txt"
block3 = []
with open(wordFile, "r") as f:
    block3 = f.read().splitlines()

# Load texts from file
if lang = 1:
	wordFile = "block4_de.txt"
elif lang = 2:
	wordFile = "block4_en.txt"
block4 = []
with open(wordFile, "r") as f:
    block4 = f.read().splitlines()

# Merge blocks
block_texts=[]
block_texts[0:]=block1
block_texts[1:]=block2
block_texts[2:]=block3
block_texts[3:]=block4

# Load texts from file
if lang = 1:
	wordFile = "item1_de.txt"
elif lang = 2:
	wordFile = "item1_en.txt"
item1 = []
with open(wordFile, "r") as f:
    item1 = f.read().splitlines()

# Load texts from file
if lang = 1:
	wordFile = "item2_de.txt"
elif lang = 2:
	wordFile = "item2_en.txt"
item2 = []
with open(wordFile, "r") as f:
    item2 = f.read().splitlines()

# Load texts from file
if lang = 1:
	wordFile = "item3_de.txt"
elif lang = 2:
	wordFile = "item3_en.txt"
item3 = []
with open(wordFile, "r") as f:
    item3 = f.read().splitlines()

# Load texts from file
if lang = 1:
	wordFile = "item4_de.txt"
elif lang = 2:
	wordFile = "item4_en.txt"
item4 = []
with open(wordFile, "r") as f:
    item4 = f.read().splitlines()

outdir = "~/git_repos/Questionnaires-PsychoPy"

#----------------------------
#Obtain subject number/info
userVar = {'1. Subject':'Enter','4. Age':'Enter age','5. Gender':'M or F','6. Handedness':'R or L'}
dlg = gui.DlgFromDict(userVar)
subject = int(userVar['1. Subject'])
age = (userVar['4. Age'])
gender = userVar['5. Gender'].lower()

if gender == 'f':
    gender = '1'
elif gender == 'm':
    gender = '2'

#Use this to create an output file
if not os.path.isdir('Data'):
    os.makedirs('Data')  # if this fails (e.g. permissions) we will get error

study_output_name = 'Data'+os.path.sep+'VVIQ_'+(userVar['1. Subject'])+'.txt'
study_output = open(study_output_name, 'a')
study_output.write('{}\t{}\n'.format('Age: ',age))
study_output.write('{}\t{}\n'.format('Gender: ',gender))
study_output.write('Subject\tBlock\tItem\tResponse\n')

#-------------------------------
#Define objects and instruction trials
win = visual.Window([600, 400], fullscr=False, color="black", units='pix')
instrStim = visual.TextStim(win,text='instructions', height=24, color='white',pos=[0,0],font='Lucida Console', wrapWidth=700)
win.setMouseVisible(False)
fixation = visual.TextStim(win,text='+', height=32, color='white',pos=[-200,-300],font='Lucida Console')


##################################################################################################################################################################################
#BEGIN EXPERIMENT
#Present instructions
instrStim.setText(instruct)
instrStim.draw()
win.flip()
core.wait(3)
act = event.waitKeys(10000,'space,delete')

if act[0] == 'delete':
        sys.exit()

#Present blank screen
win.flip()
core.wait(.15)

for block_num, block_text in enumerate(block_texts):

#present fixation cross
    fixation.setPos([0,0])
    fixation.draw()
    win.flip()
    core.wait(.5)
    
    instrStim.setText(block_texts[block_num:])
    instrStim.draw()
    win.flip()
    core.wait(1)
    act = event.waitKeys(10000,'space,delete')

    if act[0] == 'delete':
        sys.exit()

    # loop through items
    for item_num in range(4):
        # present item
        print item_num
        if item_num==0:
            items=item1
        elif item_num==1:
            items=item2
        instrStim.setText(items[item_num])
        instrStim.draw()
        win.flip()
        core.wait(3)

        # Play sound

        #Keyboard responses
        resp_raw= event.waitKeys(10000,'1,2,3,4,5,delete')
        if resp_raw  == 'delete':
            sys.exit()

        # Print out response
        study_output.write('{}\t{}\t{}\\t{}\n'.format(subject, '1', str(block_num), str(item_num), str(resp_raw)))


#Present blank screen
win.flip()
core.wait(.15)

# Close output file
study_output.close()

# Goodbye screen
instrStim.setText(thank_you_instr)
instrStim.setHeight(24) 
instrStim.draw()
win.flip()

event.waitKeys(10000,'space')
sys.exit()